# -*- coding: utf-8 -*-
"""
@author: Cristiano Ferro %(username)
Created on %(date) 
"""
# ----------------------------
# Matrix exercise
m = [
     [1,2,3],
     [4,5,6],
     [7,8,9]
     ]

# ----------------------------
# 1 - print first row
def firstRow(list):
    return list[0]
print(firstRow(m))

# ----------------------------
# 2 - print first column in single line
def firstCol(list):
    col = []
    for i in list:
        col.append(i[0])
    return col
print(firstCol(m))

# ----------------------------
# 3 - print main diameter 1, 5, 9
def mainDiam(list):
    middle = 0
    diam = []
    for i in list:
        diam.append(i[middle])
        middle += 1
    return diam
print(mainDiam(m))

# ----------------------------
# 4 - print another diameter 3 5 7
def otherDiam(list):
    startAtEnd = -1
    invDiam = []
    for i in range(len(list)):
        invDiam.append(list[i][startAtEnd])
        startAtEnd -= 1
    return invDiam
print(otherDiam(m))
# ----------------------------
# 5 - Calculate Sum of rows
def sumRows(list):
    sumOfRows = 0
    sumRowsList = []
    for i in list:
        sumOfRows = 0
        for y in i:
            sumOfRows += y
        sumRowsList.append(sumOfRows)
    return sumRowsList
print(sumRows(m))

# m = [
#      [1,2,3],
#      [4,5,6],
#      [7,8,9]
#      ]

# ----------------------------
# 6 - Calculate Sum of columns
def sumCols(list):
    sumColsList = []
    for total in range(len(list[0])):
        sumOfCols = 0
        for i in list:
            sumOfCols += i[total]
        sumColsList.append(sumOfCols)
    return sumColsList

print(sumCols(m))

# Transpose
def transpose(list):
    innerList = []
    transpList = []
    for total in range(len(list[0])):
        innerList = []
        for i in list:
            innerList.append(i[total])
        transpList.append(innerList)
    return transpList

print(transpose(m))