# -*- coding: utf-8 -*-
# ----------------------------
# @author: Cristiano Ferro (utilizador)
# Create on Wed Oct 21 08:48:42 2020
# ----------------------------
# 
# Lists

# ----------------------------
# Slide 15
a = [5, 7, 12]
print(a[0])
print(a[1])
print(a[2])

print(type(a))
print(len(a))

b = [1.618, 'Python Course', 0, {'Joe':21}, [], (3, 6, 9)]

# ----------------------------
# Slide 16
# dir(a) # Print in console to see possible functions
# dir(list) #Same here

# ----------------------------
# Slide 17 - checking indexes by index or negative index
my_list = ['p','r','o','b','e']

print(my_list[0])
print(my_list[-5])
print(my_list[1])
print(my_list[-4])
print(my_list[2])
print(my_list[-3])
print(my_list[3])
print(my_list[-2])
print(my_list[4])
print(my_list[-1])

# ----------------------------
# Slide 18

print(a.index(12)) # gives index of element
print(a[2]) # accesses by index
a[1] = 8 #changes the element of that index

# ----------------------------
# Slide 19
# Strings are immutable - can't be changed with indexing
s = 'sara'
print(s[1])
# s[1] = '@' # this won't work:
# can only be done by reassigning the string - creating it again
s = 'sara'
transition = ''
replaced = False
for i in s:
    if i == 'a' and not replaced:
        transition += '@'
        replaced = True
    else:
        transition += i
s = transition
print(s)

# ----------------------------
# Slide 20
a = [1, 2]
b = [2, 1]
print(a == b) # False

# ----------------------------
# Slide 21
# Printing lists

friends = ['Hamed', 'Josi', 'Stefan']
for f in friends:
    print(f)

for i in range(3):
    print(friends[i])
    
for i in range(len(friends)):
    print(friends[i])

# ----------------------------
# Exercise 1
# Slide 23
# solution a.

my_list = [1, 2, 23, 4, 'word']
for i in [0, 1, 2, 3, 4]:
    if i == len(my_list)-1:
        break
    print(my_list[i], my_list[i+1])

# solution b.
my_list = [1, 2, 23, 4, 'word']
for i in range(len(my_list)-1):
    print(my_list[i], my_list[i+1])

# ----------------------------
# Slide 24
    
L = [3, 6, True, 'ali', 2.7, [5,8], 'tubarão', 'redbull dá-te asas',
     'já um {i} com asas não convém nada']
print(L[-2], ',', L[-1].format(i = L[-3]))


# ----------------------------
# Slide 27, 28, 29, 30

a = [7, 5, 30, 2, 6, 25]
print(a[1:4])
print(a[:3])
print(a[3:])
print(a[3:0])
print(a[3:0:-1])
print(a[::-1])
print(a[0:7:2])
print(a[6:0:-2])
print(a[50:0:-2])
print(a[:0:-2])
a[3:5] = [14, 15]

# ----------------------------
# Slide 31
a = [4, 7]
b = a*2
print(b)

a = [1, 2]
b = ['a', 'b', 'c']
c = a + b
print(c)

# ----------------------------
# Slide 32 - check membership
a = [7, 5, 30, 2, 6, 25]
print(14 in a)
print(14 not in a)

a = [3, [109, 27], 4, 15]

print(a[1]) # print(a(1)) gives an error
print(a[1][1]) # print(a[1, 1]) gives an error but would work for a 2D numpy array
print(len(a))

# ----------------------------
# Exercise 2
# Find the maximum value in the list
a = [7, 5, 30, 2, 6, 25]
maxVal = 0 # or maxVal = a[0]
for i in a:
    if i > maxVal:
        maxVal = i
    print('max value:', maxVal)

# Now the total sum
numSum = 0
for i in a:
    numSum += i
    print(numSum)
    
# Check with methods
print('max',max(a))
print('min',min(a))
print('sum',sum(a))

a.pop()
print('last item removed', a)
a.count(2)
a.insert(-1, 2) # insert another 2 at end
a.count(2) # count again

a.remove(2)
a.count(2)

x = [10, 15, 12, 8]
a = x.pop()
print(x)
print(a)

y = ['a', 'b', 'c']
p = y.pop(1)
print(p)
print(y)


del(a)
try:
    print('list deleted' if a else "list still exists")
except:
    print('list deleted and the if would throw an error')
    
a = [5, 9, 3]
# del a[1]
# b = del a[1] # Throws an error that I can't even catch with try... Just don't "try" this at home!

print(a)

a = [0, 1, 2, 3, 4, 5, 6]
del a[2:4]
print(a)

b = [a.reverse()]
x = [1, 2, 3, 5]
x.extend([5])
print(x)

a = [1, 2, 3]
print(a[::-1])
a.reverse()
print(a)

b = a.reverse()
print(b)

a = [2, 4, 3, 5, 1]
a.sort()
print(a)

x = [1, 2, 3]
y = [4, 5]
x.extend(y)
print(x)
print(len(x))
print(len(y))

a = [1, 2, 3]
a.append(4)
print(a)

x = [1, 2, 3]
y = [4, 5]
x.append(y)
print(x)
print(len(x))
print(len(y))

a = []
for i in range(4):
    a.append(i)
print(a)

a = [1, 2, 3]
a.clear()
print(a)
print(len(a))

# append vs extend: append adds a list to a list, extend only gives the value

a = [1, 2, 3]
c = a
a[0] = 56
b = a.copy()
print(b)
b[0] = 0
b[0] = 99
d = a[::]
e = a[0]
a[0] = 88
d[0] = 22

x = 2
y = x
y += 1
print(x)
print(y)

x = []
y = x
y.append(5)
print(x)
print(y)

# NOTE: in numpy slicing doesn't create a new one, it saves in memory!

# [<M operation> for M in a]
k = [i for i in range(4)]
print(k)

k = [i*2 for i in range(4)]
print(k)

k = [i*i for i in range(3,6)]
print(k)

a = [1, -2, 5, -56, 8]
b = [abs(i) for i in a]
print(b)

import math

a = [round(math.pi,i) for i in range(1,5)]
print(a)

a = ['$ali', 'sara$']
b = [i.strip('$') for i in a]
print(b)

a = [11, 8, 14, 20, 2]
b = [i  for i in a if i < 10]

print(b)

# Exercise 3

a = [1, 2]
b = [1, 4, 5]
c = []
for i in a:
    for j in b:
        if i != j:
            c.append((i,j))
print(c)

# Loop process 
# i = 1, j = 1, C = []
# i = 1, j = 4, c = [(1,4)]
# i = 1, j = 5, c = [(1,4),(1,5)]
# ...
# [(1, 4), (1, 5), (2, 1), (2, 4), (2, 5)]
    
a = [2.6, float('NaN'), 4.8 , 6.9 , float('NaN')]
b = []
import math

for i in a:
    if not math.isnan(i):
        b.append(i)
print(b)

# If you want to change length of a list, dict or set in a loop and want
# to remove/add some objects you need to check the iterable variable in 
# your loop
# If you used the same list/dict or set you need to make a copy

a = [1, 2, 3, 4]
c = a.copy()
for i in c:
    if i > 0:
        c.pop()
print(a)
print(c)

        