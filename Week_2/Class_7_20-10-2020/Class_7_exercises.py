# -*- coding: utf-8 -*-
# ----------------------------
# @author: Cristiano Ferro (utilizador)
# Create on Tue Oct 20 08:53:27 2020
# ----------------------------
# Class / Presentation exercises



# ----------------------------
# Exercise 1

import numpy as np
import pandas as pd #wasn't needed after all
# Exercise - import matplotlib.pyplot without using a dot
from matplotlib import pyplot as plt
data = np.array([-20, -3, -2, -1, 0, 1, 2, 3, 4])
plt.boxplot(data)

# ----------------------------
# Exercise 2
# Slide 31, 32
# What do these functions do

# with math module
import math
# returns remainder
print(math.fmod(9 ,4))
# returns highest common factor
print(math.gcd(30 ,4))
# returns absolute value
print(math.fabs(-4))

# now with random module
import random
# returns a random number from a range
print(random.randint(1, 5))
# returns a pseudo number from a range
print(random.choice([1, 5]))
a = [1, 2, 3, 4]
# returns a shuffled list
random.shuffle(a)
print(a)

# ----------------------------
# Exercise 3

# Exercise: access minutes, determine if it's an odd or even number
import datetime
now = datetime.datetime.now()

if now.minute % 2 != 0:
    print("Odd minute")
else:
    print("Not an odd minute")

# Slide method
m  = datetime.datetime.today().minute
for i in range(1, 61, 2):
    if m == 1:
        print("Odd minute")
        break
    else:
        print("Not an odd minute")
        break

if now.minute in range(1, 61, 2): # even: range(0, 60, 2)
    print("Odd minute")
else:
    print("Not an odd minute")

# Other method
odds_list = [i for i in range(1, 60, 2)]