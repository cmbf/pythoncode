# -*- coding: utf-8 -*-
"""
@author: Cristiano Ferro %(username)
Created on %(date) 
"""
# ----------------------------
# Standard Library & Modules

# ----------------------------
# Slide 16
# type this in console, line by line
# sum??
# from numpy import *
# sum??

# ----------------------------
# Slide 20, 21, 22
# Testing module imports
# Run 1) and 2) separately, not at the same time
# 1)
import math
print(math.pi)

# 2)
import math as m
print(m.pi)
print(math.pi)
# 3)
from os import getcwd
print(getcwd())
# 4)
from os import getcwd as gc
print(gc())


# ----------------------------
# Slide 27
import random
import math
for i in range(5):
    print(random.randint(1, 25))

print(math.pi)

# ----------------------------
# Slide 28
# dir()
import math
print(dir(math))

s = 'a'
print(dir(s))

# ----------------------------
# Slide 33

import math
print(math.sqrt(4))         #2.0
print(math.trunc(2.3))      #2
print(math.floor(2.3))      #2
print(math.ceil(2.3))       #3
print(math.factorial(4))    #24 , 4! = 4*3*2*1
print(math.log2(32))        #5.0
print(math.log10(100))      #2.0
print(math.e)               #2.7
print(math.log(32))         #3.46
print(math.sin(5))          #-0.9
print(math.fmod(9,4))       #1.0 , 9%4
print(math.gcd(30,4))       #2 , greatest common divisor
# The difference of fabs and abs is that fabs always returns a float
print(math.fabs(-4))        #4.0 , float abs
print(abs(-4))  	        #4
print(math.pow(2,3))        # 8.0
print(pow(2,3))             # 8
print(math.pi)              # 3.1415926...
print(f'{math.pi :.2f}')    # 3.14

# ----------------------------
# Slide 35

import sys
print(sys.version) # 3.8.5
print(sys.platform) # win32
import platform
print(platform.release())                  # 10 -> it means windows 10

# ----------------------------
# Slide 36

import datetime
now = datetime . datetime . now()
print(now)                          # ex: 2020-10-20 11:30:47.724484
print(now.year)                     # ex: 2020
print(now.month)                    # ex: 10
print(now.day)                      # ex: 20
print(datetime.datetime.today())    # ex: 2020-10-20 11:31:45.597811
print(type(now))                    # datetime.datetime