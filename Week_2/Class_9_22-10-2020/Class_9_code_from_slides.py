# -*- coding: utf-8 -*-
"""
@author: Cristiano Ferro %(username)
Created on %(date) 
"""
# ----------------------------
# Slide 15

t = ('English', 'History', 'Mathematics')

print(t)
print(type(t))
print(len(t))

# ----------------------------
# Slide 16

t1 = (3)
t2 = (3,)
s1 = ('a')
s1 = ('a',)

# ----------------------------
# Slide 17
print(t[0])
print(t[1:3])
print(t.index('English'))
print('English' in t)

# ----------------------------
# Slide 18

t = ('English', 'History', 'Mathematics')
t = list(t) # Needs to convert to something else, change and then convert back - tuples are immutable
t[0] = 'art'
tuple(t)
print(t)

for i in t:
    print(f'I like to read {i}')

# ----------------------------
# Slide 19 - Review
a = (1, 2, 3)
i = 12
if i in a: # if i is a member inside of a
    print('there is a 12')

for i in a: # do something for each element
    print('doing something for an element')

# ----------------------------
# Slide 20
    
t = (1, 9, 2)
print(sum(t))
print(max(t))
print(min(t))
print(t.count(9))

# ----------------------------
# Slide 21

print(t*2)
print(t + t + t)
print((3, 6) + (9,))
print((1, 2) + (9, 6))
print(tuple(reversed(t)))

# ----------------------------
# Slide 21

t1 = (1, 2)
t1 = (2, 1)

print(t1 == t2)

t = (4, 6)
t = t + (9,)

print(t)

# ----------------------------
# Slide 33
# remove

t = (4, 7, 2, 9, 8)
x = list(t)
x.remove(2)
t = tuple(x)
print(t)

# ----------------------------
# Slide 34 - unpack

# Example 1
t = (4, 8)
a, b = t
print(a)
print(b)

# Example 2
car = ('blue', 'auto')
color, _, a = car
print(color)
print(_)
print(a)

# ----------------------------
# Slide 35

a = (1, 2)
b = (3, 4)
c = zip(a, b)
x = list(c)
print(x)
print(x[0])
print(type(x[0]))

z = ((1, 3), (2, 4))
u = zip(*z)
print(list(u))

# When Z is a dictionary it doesn't work because zip needs an iterable, with an index
# a dictionary is not an iterator
# So... if we had a dictionary, and wanted a tuple of tuples:
z = {1:3, 2:4}
print(z)
z2 = []
for k, v in z.items():
    z2.append(tuple([k, v]))
z2 = tuple(z2)
print(z2)
u = tuple(list(zip(*z2))) # needs to be converted to list after zip, can't be a tuple right away
print(u)

z = dict(z2)
print(z)

# ----------------------------
# Slide 36
a = (1, 2, 'A')
b = (3, 4, 8)
c = zip(a, b)
x = list(c)
print(x)

print(list(zip(*x)))

# minimum length between 'a' and 'range(2)' is 2
a = [11, 22, 33]
b = zip(a, range(2))
print(list(b))

# ----------------------------
# Slide 40
# how many tuples are in 'num' list

num = [8, 2, (9,3), 4, (1, 6, 7), 34]

c = 0
for i in num:
    if isinstance(i, tuple):
        continue
    c += 1
print(c)
print(len(num) - c)

# ----------------------------
# Slide 44

a = [(1, 2, 3), (4, 5, 6)]
b = [i[:-1]+(9,) for i in a]
print(b)