# -*- coding: utf-8 -*-
# ----------------------------
# @author: Cristiano Ferro (utilizador)
# Create on Thu Oct 22 08:40:48 2020
# ----------------------------

# Exercise 1
# ----------------------------
t = (4, 6)

# Solution 1
t = list(t)
t.insert(len(t), 9)
print(tuple(t))

# Solution 2
x = (4, 6)
x = list(x)
x.append(9)
print(tuple(x))

# Solution 3
y = (4, 6)
y = list(y)
y.extend([9])
print(tuple(y))


# Exercise 2
# ----------------------------

# Solution 1 - without converting to another data type
txt = "$ $ Python$#Course  $"
txt = txt.strip('\s$ ')
txt = txt.replace("#", " ")
txt = txt.replace("$", "")
print(txt)

# Solution 2 - convert to list (Solution 2 from slides)
txt2 = "$ $ Python$#Course  $"
txtList = list(txt2)
txtList2 = []
for i in txtList:
    if i == "#":
        txtList2.append(" ")
    elif i == "$" or i == " ":
        continue
    else:
        txtList2.append(i)
abc = ''
for i in txtList2:
    abc += i
print(abc)

# Solution 3 - convert to list (Solution 1 from slides)
b = "$ $ Python$#Course  $"
# a = b.strip('# $')
# a = list(b)
a = list(b)
c = a.copy()
for i in a:
    if i == "#" or i == "$" or i == " ": # i in ["#", "$"]
        c.remove(i) # a.pop(index('#'))
c.insert(c.index("C"), " ")
# print(c)

t = ''.join(c)
print(t)

# Exercise 3
# ----------------------------
A = [1, 2, "A"]
B = ('Python', 168.8, 0, 5)
C = {10, 12, 14, 16, 18, 20}
# Output expected:
print(list(zip(A,B,C)))

GG = []
BBB = ()
C = list(C)
smallerObj = min(len(A), len(B), len(C))
for i in range(smallerObj):
    my_tuple = (A[i], B[i], C[i])
    GG.append(my_tuple)
print(GG)
# nonZipped = []
# C = list(C)
# smallerObj = min(len(A), len(B), len(C))
# for i in range(smallerObj):
#     my_tuple = (A[i], B[i], C[i])
#     nonZipped.append(my_tuple)

# 1o [1]
# 2o [2]
    
# Exercise 4 - Tuple Exercise
# ----------------------------
AA = [8, 2, (9,3), 4, (1, 6, 7), 34]

varr = 0
# print(a[3] == tuple())
for i in AA:
    if type(i) == tuple:
        continue
    varr += 1
    
print(len(AA) - varr)

# ----------------------------
# Solution 2 - from class/slides
# It's the opposite approach
# And is better because it has less code

varr = 0
for i in AA:
    if type(i) == tuple:
        varr += 1
    
print(varr)


