# -*- coding: utf-8 -*-
# ----------------------------
# @author: Cristiano Ferro (utilizador)
# Create on Fri Oct 23 12:21:22 2020
# ----------------------------
# ----------------------------
# Merge 2 dictionaries together
# For same keys, sum values
# ----------------------------
# ----------------------------

d1 = {'x': 3 , 'y': 2, 'x': 1 }
d2 = {'w': 8, 't': 7, 'z': 5}

for i, j in d2.items():
    if i in d1:
        d1[i] += d2[i]
    else:
        d1.update({ i : j })

print(d1)
