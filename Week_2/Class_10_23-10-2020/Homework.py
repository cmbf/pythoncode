# -*- coding: utf-8 -*-
"""
@author: Cristiano Ferro %(username)
Created on %(date) 
"""
# ----------------------------

person = {'phone':{'home':"01-4455", 'mobile':"918-123456"},
          'children':['Olivia', 'Sophia'], 
          'age': 48, 'other':4}

print(len(person))
print(person['phone']['home'])
print(person['phone']['mobile'])
print(person['children'])
print(person['children'][0])
print(person.pop('age'))
