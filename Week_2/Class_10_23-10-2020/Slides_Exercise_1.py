# -*- coding: utf-8 -*-
"""
@author: Cristiano Ferro %(username)
Created on %(date) 
"""
# ----------------------------
# Exercise 1
e = 'abfabdcaa'
f = dict()
startNr = 4
for i in e:
    if i in f:
        continue
    f[i] = startNr
    if startNr <= 1:
        continue
    else:
        startNr = int(startNr / 2)
print(f)
# Output {'a' : 4, 'b' : 2, 'f' : 1, 'd' : 1 , 'c' : 1}
