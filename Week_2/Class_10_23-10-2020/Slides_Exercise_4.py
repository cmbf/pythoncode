# -*- coding: utf-8 -*-
# ----------------------------
# @author: Cristiano Ferro (utilizador)
# Create on Fri Oct 23 11:41:41 2020
# ----------------------------
# ----------------------------
# Calculate sum of values in dict
# Sorting exercise at end
# ----------------------------
# ----------------------------


# ----------------------------
# Solution 1
d = {'a' : 4, 'b' : 2, 'f' : 1, 'd' : 1, 'c' : 1}

sumSum = 0

for k, v in d.items():
    sumSum += v
print('sum of all:', sumSum)


# ----------------------------
# Solution 2
sumSum2 = 0

for values in d:
    sumSum2 += d[values]
    
print('sum of all:', sumSum2)

# ----------------------------
# Solution 3
sumSum3 = sum(d.values())
print('sum of all:', sumSum3)



