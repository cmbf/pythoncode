# -*- coding: utf-8 -*-
"""
@author: Cristiano Ferro %(username)
Created on %(date) 
"""
# ----------------------------
# Exercise 1
# ----------------------------

lines = "a dictionary is a datastructure\n and a set is also a datastructure."

d = {}
for i in range(len(lines.split("\n"))):
    line1 = lines.split("\n")[i].split(".")[0]
    
    s = line1.split()
    for i in s:
        d[i] = d.get(i, 0) + 1
    
print(d)