# -*- coding: utf-8 -*-
# ----------------------------
# @author: Cristiano Ferro (utilizador)
# Create on Fri Oct 23 08:41:51 2020
# ----------------------------
# ----------------------------
# Class 10 Exercises
# ----------------------------
# ----------------------------

# Slide 15
# ----------------------------
d = {
     'brand': 'cherry',
     'model': 'arizo5',
     'color': 'white'
     }
print(type(d))
print(len(d))

# Slide 17
# ----------------------------

d['year'] = '2010'

print(d['model'])
d['color'] = 'black'

# Slide 18
# ----------------------------

print(d)

x = d.get('model')
print(x)

x = d.get('cylinder')
print(x)

x = d.get('cylinder', -1)
print(x)

# Slide 19, 20, 21, 22
# ----------------------------
print(d)
print(list(d.keys()))
print(list(d.values()))
print(list(d.items()))
for k, v in d.items():
    print(k, ':', v)

d.pop('model')
print(d)

f = d.popitem()
print(d)
print(f)
print(type(f))

d.clear()
print(d)
del d

# Other code
# ----------------------------
dict_name = {"egtaet":'ert'}
a = dict_name.get("egtaet")

d1 = {'1':'one', '2': 'two', '3': 'three'}


x = '1' in d1
y = d1.get('3', False)
print(x)
print(y)

c = 1
for k, v in d1.items():
    print('key  ', c, 'is:', k)
    print('value',c ,'is:', v)
    c += 1
# print "Python Course" #Python V2.x

# Slide 23
# ----------------------------
# Solution 1
a = ['x', 'y', 'x', 'z', 'y', 'x']
d = {}
for i in a:
    if i not in d:
        d[i] = 1
    else:
        d[i] += 1
print(d)

# Solution 2 - with get
d2 = {}
for i in a:
    d2[i] = d2.get(i, 0) + 1
print(d2)

# Solution 3
# d3 = 
d3 = {}
for i in a:
    d3[i] = d3.setdefault(i, 0) + 1
print(d3)

for i in range(1, 101):
    # d3[i]:[f'{i}']
    d3.setdefault(i, str(i))
print(d3)
    
# Slide 28
# ----------------------------

a = {}
b = a
c = a.copy()
b['test'] = 'testing'
c['aaaaa'] = 'bbbbb'
print(a)
print(b)
print(c)

# ----------------------------
# Slide 40

d = {'a':4, 'b':2, 'f':1, 'd':1, 'c':1}

s = 0

for i in d:
    s += d[i]
print(s)
print(sum(d.values()))

# ----------------------------
# Sorting

import operator
k = operator.itemgetter(1) # Sort by values
print(sorted(d.items(), key = k))

k = operator.itemgetter(0) # Sort by keys
print(sorted(d.items(), key = k))

num = {
       'ali':[12, 13, 8],
       'sara':[15, 7, 14],
       'taha':[5, 18, 13]
       }
d = {k: sorted(v) for k, v in num.items()}
print(d)

# ----------------------------
# Slide 45

d1 = {'x' : 3, 'y' : 2, 'z' : 1}
# d1 = {'1':'one', '2': 'two', '11': 'eleven'}
d2 = {'w' : 8, 't' : 7, 'z' : 5}
# d1 = {'1':'one', '3': 'three', '4': 'four'}

# The following loop does this:
# d = d1.copy()
# d1.update(d2)
# d = {}
for i in (d1,d2):
    print(i, "i")
    d.update(i)
print(d)

d = {**d1, **d2}
print(d)
