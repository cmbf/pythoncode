# -*- coding: utf-8 -*-
"""
@author: Cristiano Ferro %(username)
Created on %(date) 
"""
# ----------------------------
# Previous week revisions

# ----------------------------
# Slide 17
# Variable Examples
name ="Josi"
age = 31

print('%s is %d years old' % (name, age))
print('{} is {} years old'.format(name, age))
print(f'{name} is {age} years old')
      
# Simplified:
print(name, "is", age, "years old")
print(name + " is " + str(age) + " years old") #all should be strings

# Change the code for different input age types without raising
# errors : "31", "31.5", 31.5
print(name, "is", int(float(age)), "years old")

# ----------------------------
# Slide 18
# variable names
print ("a2" . isidentifier() ) # True
print ("2a" . isidentifier() ) # False - can't start with a number

# ----------------------------
# Slide 19
from keyword import iskeyword
print(iskeyword("if")) #It's a reserved word -> can't be used for variable names

# ----------------------------
# Slide 20
# Variables Name Examples
print("a2".isidentifier())      # True
print("2a".isidentifier())      # False
print("_myvar".isidentifier())  # True
print("my_var".isidentifier())  # True
print("my-var".isidentifier())  # False
print("my var".isidentifier())  # False
print("my$".isidentifier())     # False
print("my#".isidentifier())     # False

# ----------------------------
# Slide 21
# Multi assignment

a = 5
b = 1
print('Five plus one is a {a + b}')
print(f'Five plus one is a {a + b}')
a = b = c = 5
print(a, b, c)

x = 1
y = 2
y, x = x, y
print(x)
print(y)

# ----------------------------
# Slide 22
# Data Types

print(type(-11))
print(type(7))
print(type(0))
print(type(33))
print(type(1000))
print(type(3 + 2j))
print(type(-4 -7j))
print(type(78.6))
print(type(0.382))
print(type(0.0))
print(type(-1.272))
print(type({'X':13, 'Y':33}))
print(type(True))
print(type(False))
print(type(1>0))
print(type(0.618==0.618))
print(type({'A', "Python", '11'}))
print(type([0.88, 'Python', True, 0, (2, 1)]))
print(type((3, -1, 'Python', True, 0)))

# ----------------------------
# Slide 24
# Data Types Examples

s = "Python course"
print(type(s))

i = 2
print(type(i))

f = 2.5
print(type(f))

c = 2 + 3j
print(type(c))

# ----------------------------
# Slide 24
# Data Types Examples

print(bool(5))
print(bool(-1))
print(bool('cris'))

print(bool(0))
print(bool(''))

print(bool([]))
print(bool({}))
print(bool(()))
b = True
c = 52
print(type(b))
print(type(c))

# ----------------------------
# Slide 25
# Data Types Examples

l = ["apples", "grapes", "oranges"]
print(type(l))

t = ("apples«", "banana", "cherry")
print(type(t))

d = {'id': '123', 'name': 'cris'}
print(type(d))

s = {'apple', 'banana', 'cherry'}
print(type(s))

# ----------------------------
# Slide 26
# Input, Outpu

a = int(input('Enter a:'))
b = int(input('Enter b:'))
c = a + b
print(c)

n = 12.5
print('%i' % n)
print('%f' % n)
print('%e' % n)

# ----------------------------
# Slide 29
# Operators

print(1 + 3)
print(5 - 3)
print(2 * 3)
print(3 / 2)
print(3 // 2)
print(17 % 5)
print(2 ** 3)
print(0 ** 0)
print(6 ** 0)

# ----------------------------
# Slide 30
# Operators Precedence Examples

print(8 - 2 * 3)
print(1 + 3 * 4 / 2)
print(16 / 2 ** 3)
print(2**2**3) #256, 2**3 is calculated first

# ----------------------------
# Slide 31
# Augmented Assignment Operator Examples

x = 4
x += 2
print(x)

y = 8
y //= 2
print(y)

# ----------------------------
# Slide 32
# Comparison and Logical operators and Short-Circuit Examples
print(2 == 3)
print(2 != 3)
print( 2 < 3)

print(1<3 or 4>5)
print(1<3 and 4>5)
print(not 1<3)

print(1 >= 2 and (5/0) > 2) # False - second part doesn't get to execute
# print(3 >= 2 and (5/0) > 2) # Error

# ----------------------------
# Slide 33
# Membership and Bitwise Operators examples

x = [1, 2, 3, 4, 5]

print(3 in x)
print(24 not in x)
a = 13
print(bin(a))

b = 14
print(bin(b))

c = a | b
print(bin(c))

c = a & b
print(bin(c))

c = a ^ b
print(bin(c))

# ----------------------------
# Slide 34
a = 13
print(a << 1)

a = 20
print(a >> 1)

a = 18
print(a >> 2)

# Operations on String Examples
s1 = 'Python'
s2 = ' Course'
s3 = s1 + s2
print(s3)

s = 'Sara'
print(2*s)

# ----------------------------
# Slide 36
# Conditional statements & expression

import math

n = -16
# n = int(input('enter:'))
if n < 0:
    n = abs(n)

print(math.sqrt(n))

# ----------------------------
# Slide 37

a = 5
if True:
    a = 6
print(a)

a = 20
if a % 2 == 0:
    print('even')
else:
    print('odd')


# ----------------------------
# Slide 38
x = 3
y = 2
if x == 1 or y == 1:
    print('ok')
else:
    print('no')

names = ['sara', 'taha', 'cris']
if 'ali' in names:
    print('found')
else:
    print('not found')

# ----------------------------
# Slide 39

a = 2
b = 5
 
if a < b:
    m = a
else:
    m = b

m = a if a < b else b # Conditional expression

# ----------------------------
# Slide 40

my_list = ['a', 'e', 'o', 'i', 'u']

if 'o' in my_list:
    s = 'yes'
else:
    s = 'no'

s = 'yes'  if ('o' in my_list) else 'no' # Conditional expression
print(s)

# ----------------------------
# Slide 41
x = 2
y = 6

z = 1 + ( x if x > y else y + 2)

print(z)

grade = 12
s = 'fail' if grade < 10 else 'pass'
print(s)

# ----------------------------
# Slide 42

score = 75

if score >= 90:
    l = 'A'
else:
    if score >= 80:
        l = 'B'
    else:
        if score >= 70:
            l = 'C'
        else:
            l = 'D'
print(l)

# ----------------------------
# Slide 43
score = 75
if score >= 90:
    l = 'A'
elif score >= 80:
    l = 'B'
elif score >= 70:
    l = 'C'
else:
    l = 'D'
print(l)


# ----------------------------
# Slide 45
# Loops
for j in range(5,10,2):
    print(j, end = ' ')

s = 'Python'
for ch in s:
    print(ch)
    
for _ in range(3):
    print('hello')

# ----------------------------
# Slide 46

for j in range(4):
    print(i, end = ' ')
    
for i in range(3, 8):
    print(i, end= '')

# ----------------------------
# Slide 47

word = 'python'
c = 0
for i in word:
    c += 1
print(c)

for i in range(9, 2, -3):
    print(i, end = ' ')
    
# ----------------------------
# Slide 48

word = 'alireza'
c = 0
for i in word:
    if i == 'a':
        c += 1
print(c)

# ----------------------------
# Slide 49

name = 'cris'
v = 'aeiou'
c = 0
for ch in name:
    if ch in v:
        print(ch)
        c += 1

print(c)

# ----------------------------
# Slide 50

name= 'cris'
v = 'aeiou'
a = [ch for ch in name if ch in v]
print(a)

for i in range(1, 4):
    for j in range(2,4):
        print(j, end = ' ')
    print()

# ----------------------------
# Slide 51

for i in range(5):
    if i == 3:
        break
    else:
        print(i, end = ' ')

for i in range(5):
    if i == 3:
        continue
    else: print(i, end = ' ')

# ----------------------------
# Slide 52

i = 1
while i <= 3:
    print(i, end = ' ')
    i += 1

n = 7
while n >= 3:
    print(n, end= ' ')
    n -= 1

# ----------------------------
# Slide 53
    
s = 'abcdef'
i = 0
while True:
    if s[i] == 'd':
        break
    print(s[i], end = ' ')
    i += 1

# ----------------------------
# Slide 54

n = 8
while n > 2:
    n -= 1
    if n == 5:
        break
    print(n, end = ' ')
    
n = 8
while n > 2:
    n -= 1
    if n == 5:
        continue
    print(n, end = ' ')

# ----------------------------
# Slide 55

# not PEP8 standard
n = 1
while n <= 3: print(n); n += 1
 
# it is PEP8 standard
n = 1
while n <= 3:
    print(n)
    n += 1

# ----------------------------
# Slide 57
# Fame - guess the number between 0 to 9

import random

n = random.randrange(0, 10)
f = 'no'

print(n)

while f == 'no':
    a = int(input('Game: guess number between 0 to 9: '))
    if a < n:
        print('increase')
    elif a > n:
        print('decrease')
    else:
        print('Correct, You won')
        f = 'yes'

print('Thankyou')