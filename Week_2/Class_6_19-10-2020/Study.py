# -*- coding: utf-8 -*-
# ----------------------------
# @author: Cristiano Ferro (utilizador)
# Create on Mon Oct 19 08:41:04 2020
# ----------------------------
# study with stuff from stackoverflow

# how to convert a string to variable
# using exec() function
x='buffalo'    
exec("%s = %d" % (x,2))

print(buffalo)

# ----------------------------
# how to turn a variable into a string
from varname import Wrapper

foo = Wrapper(dict())

# foo.name == 'foo'
# foo.value == {}
foo.value['bar'] = 2

# ----
# other option? probably needed to install package: pip3 install python-varname==0.1.5
from varname import nameof

foo = dict()

fooname = nameof(foo)
# fooname == 'foo'

# ----
n_jobs = 62
d = n_jobs

print(nameof(d))# will return d, instead of n_jobs
# nameof only works directly with the variable

[print(item) for item in [1, 2, 3]]

