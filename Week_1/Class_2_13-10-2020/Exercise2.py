# -*- coding: utf-8 -*-
# ----------------------------
# @author: Cristiano Ferro
# Created on
# ----------------------------
# Exercise 2
# Part a.
ageGiven = int(input("How old are you?"))
print("Your age is", ageGiven)

# ----------------------------
# Part b.
if(16 <= ageGiven <= 65):
    print("Have a good day at work")
elif(ageGiven < 16):
    print("You are too young to work, come back to school")
else:
    print("You have worked enough, Let’s Travel now")