# -*- coding: utf-8 -*-
# ----------------------------
# @author: Cristiano Ferro
# Created on 13/10/2020
# ----------------------------

# ----------------------------
# 1. Answer what type of object(variable) should be used to store each of the 
# following information:
print('\nExercise 1:')
print("a. A person's age.")
print('Ans: int')
print('b. The area of your yard in square meters.')
print('Ans: int')
print('c. The amount of money in your bank account')
print('Ans: int')
print('d. The name of your favorite songs.')
print('Ans: string')

# ----------------------------
# 2. Write a Python program that accepts a word from the user and reverse it
print('\nExercise 2:')
def reverser():
    wordFromUser = str(input("Tell me a word so I can mirror it: "))
    stringLength = len(wordFromUser)
    # testing purposes
    # print(stringLength)
    letter = stringLength - 1
    reversedWord = ''
    while letter >= 0:
        reversedWord += wordFromUser[letter]
        letter -= 1
    print(reversedWord)

# calls function of exercise 2
print("commented for quicker testing purposes")
# print("Calling the reverser function")
# reverser()
    
# ----------------------------
# 3. Initialize the string “abc” on a variable named “s”:
print('\nExercise 3:')
s = 'abc'

# a. Use a function to get the length of the string.
print('\nExercise a.:')
len(s)
# b. Write the necessary sequence of operations to transform the string “abc” 
# in “aaabbbccc”
print('\nExercise b.:')
def repeat3Times(inStr):
    repeatedChars = ''
    for i in s:
        repeatedChars += i*3
    inStr = repeatedChars
    print(inStr)
    
# calls function of exercise 3
print("Calling the repeat3Times function")
repeat3Times(s)

# ----------------------------
# 4. Initialize the string “aaabbbccc” on a variable named “s”:
print('\nExercise 4:')
s = "aaabbbccc"
# a. Use a function that allows you to find the first occurrence of “b” in the string,
# and the first occurrence of “ccc”.
print('\nExercise a.:')
def findFirst():
    bIndexPos = s.index('b')
    cccIndexPos = s.index('ccc')
    print("First ocurrence of b", bIndexPos)
    print("First ocurrence of ccc", cccIndexPos)

# calls function of exercise 3
print("Calling the findFirst function")
findFirst()
# A more obvious way to do it
# s.find('b')
# s.find('ccc')


# b. Use a function that allows you to replace all occurrences of “a” to “X”, and
# then use the same function to change only the first occurrence of “a” to “X”.
print('\nExercise b.:')
def replaceAWithX(toBeReplaced):
    replacedStr = toBeReplaced.replace('a','X')
    return replacedStr
print(replaceAWithX(s))

# Determine what to replace and with what
stringa = 'a'
replaceByX = 'X'

#Set boolean "onlyFirstOcurrence" to False to replace all occurrences of “a” to “X”
#or to True to change only the first occurrence of “a”
def replace2ndTry(toBeReplaced, onlyFirstOcurrence, occurrence, replacement):
    changedFirstOccurrence = False
    stringLength = len(toBeReplaced) - 1
    letterIndex = 0
    replacedString = ''
    while letterIndex <= stringLength:

        if(onlyFirstOcurrence and not changedFirstOccurrence and toBeReplaced[letterIndex] == occurrence):
            # trocar a por x
            replacedString += replacement
            # variavel changedFirstOccurrence fica true
            changedFirstOccurrence = True
            letterIndex += 1
        elif(not onlyFirstOcurrence and toBeReplaced[letterIndex] == occurrence):
            replacedString += replacement
            letterIndex += 1
        else:
            replacedString += toBeReplaced[letterIndex]
            letterIndex += 1
            # For testing purposes:
            # print(letterIndex)
            # print(replacedString)
            # print(toBeReplaced)
    return replacedString
    
print(replace2ndTry(s,False,stringa,replaceByX))

# 5. Starting from the string “aaa bbb ccc”, what sequences of operations do you need to
# arrive at the following strings?
# (5.)1.
print('\nExercise 5.1:')

replacedAs = replace2ndTry(s,False,'a','A')

print("replaced As:", replacedAs)
replacedABs = replace2ndTry(replacedAs,False,'b','B')
print("replaced As and Bs:", replacedABs)
replacedABCs = replace2ndTry(replacedABs,False,'c','C')
print("replaced ABCs:", replacedABCs)
#index of 1st B and 1st C
#add space when indexes match

firstB = replacedABCs.index('B')
firstC = replacedABCs.index('C')

# Function adds 2 spaces in wherever indexes the user specifies
def add2Spaces(index1, index2, strInput):
    stringLength = len(strInput) - 1
    letterIndex = 0
    finalStr = ''
    while letterIndex <= stringLength:
        if letterIndex == index1 or letterIndex == index2:
            finalStr += ' '
        finalStr += strInput[letterIndex]
        letterIndex += 1
    return finalStr
spacesForABCs = add2Spaces(firstB, firstC, replacedABCs)
print("here goes ",add2Spaces(firstB, firstC, replacedABCs))
# (5.)2.
print('\nExercise 5.2:')
replaceBswithbs = replace2ndTry(spacesForABCs,False,'B','b')
print("replaced Bs with bs:", replaceBswithbs)

# 6.
print('\nExercise 6:')
# a is 10
# b is 10
# c is 10
# d is 9
a = 10
b = a
c = 9
d = c
c = c + 1
print('a', a,'b', b, 'c', c, 'd', d)

# 7.
print('\nExercise 7:')
x,y = 2, 10
x,y = y,x
print('x',x,'y',y)

# 8.
# One possible corrected code
print ("Enter a number :")
a = int ( input ())
if a % 2 == 0:
    if a < 100:
        print ("the number is even and smaller than 100")
    else:
        print ("The number is even and equal or higher than 100")
else:
    if a < 100:
        print ("The number is odd and smaller than 100")
    else:
        print ("The number is odd and equal or higher than 100")



