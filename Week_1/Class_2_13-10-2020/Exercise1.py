# -*- coding: utf-8 -*-
# ----------------------------
# @author: Cristiano Ferro (criso)
# Created on Tue Oct 13 09:46:13 2020
# ----------------------------
# Exercise 1
guessnumber = 30
correctWasGiven = False

# ----------------------------
while not correctWasGiven:
    numberGiven = int(input("Hello hello, can you guess this number?\n"))
    if (numberGiven < guessnumber):
        print("Please, guess higher")
    elif(numberGiven > guessnumber):
        print("Please, guess lower")
    else:
        print("Congrats, you found the answer!!")
        correctWasGiven = True

