# -*- coding: utf-8 -*-
# ----------------------------
# @author: Cristiano Ferro
# Created on 
# ----------------------------
# Exercises from the powerpoint

# ----------------------------
# The exercises from the slide 7
# ----------------------------
# Exercise 1
# 
print((3/2))    # Divides 3 by 2
print((3//2))   # Divides 3 by 2 and rounds the result
print((3 % 2))  # Leaves the remainder
print((3**2))   # Does exponential multiplication

# ----------------------------
# Exercise 2
# Average exercise
number1seq1 = 2
number2seq1 = 4
print("Average of sequence 1:", (number1seq1 + number2seq1) / 2)

number1seq2 = 4
number2seq2 = 8
number3seq2 = 9

print("Average of sequence 2:", (number1seq2 + number2seq2 + number3seq2) / 3)

number1seq3 = 12
number2seq3 = 14/6
number3seq3 = 15

print("Average of sequence 3:", (number1seq3 + number2seq3 + number3seq3) / 3)

# ----------------------------
# Exercise 3
# Sphere volume calculation
import math
pinumber = math.pi
sphereRadius = float(input("Please, Enter Radius:"))
const = 4/3
power = 3
def esfera(pi, radius, const, power):
    # volume = 4/3 * pinumber * sphereRadius**3
    # print("Radius of sphere:", f"{volume:.3f} U")
    print("Radius of sphere:", f"{const*pinumber*(sphereRadius**power):.3f} U")

volume = esfera(pinumber, sphereRadius, const, power)

# ----------------------------
# Exercise 4
# Check if number is even or odd
numList = [1, 5, 20, 60/7]
def isItOdd(num):
    for i in num:
        if i % 2 == 0:
            print(i, "is even")
        else:
            print(i, "is odd")
    # print(1%2)
    # print(5%2)
    # print(20%2)
    # print((60/7)%2)

isItOdd(numList)


# ----------------------------
# Slide 12
# Comparisons using print

print(4==5)
print(6>7)
print(15<100)
print('hello' == 'hello')
print('hello' == 'Hello')
print('dog' != 'cat')
print( True == True)
print(True != False)
print( 42 == 42.0)
print( 42 == '42')
print('apple' == 'Apple')
print('apple' > 'Apple')
print('A unicode is', ord('A') ,' ,a unicode is' , ord('a')) #unicode

# ----------------------------
# Slide 14
# What is the minimum age to drive in Portugal?

minimum_age = 18
age = 23
if age > minimum_age:
    print("Congrats, you can get your license")
else:
    print("Please, come back in {0} years" .format(minimum_age - age))

# ----------------------------
# Slide 16
# Continuation of previous
minimum_age = 18
user_age = 23
if age < minimum_age:
    print("Please, come back in {0} years".format(minimum_age - user_age))
elif age == 150 :
    print("Probably, you can not drive anymore. Please, make an appointment with your doctor.")
else :
    print("Congrats, you can get your license.")

# ----------------------------
# Slide 15
# Getting Access
name = "Cris"
password = 'dontusethispassword'

if name == 'Josi':
    print('Hello Josi')
if password == 'swordfish':
    print('Access granted.')
else:
    print('Wrong password.')

# ----------------------------
# Slide 20
# Comparisons using print
print(4 < 5) and (5 < 6)
print(4 < 5) and (9 < 6)
print(1 == 2) or (2 == 2)
print(2 + 2 == 4 and not 2 + 2 == 5 and 2 * 2 == 2 + 2)


# ----------------------------
# Code using conditionals
# Slide 23

a = 3
b = 2

if a == 5 and b > 0:
    print('a is 5 and',b ,'is greater than zero.')
else:
    print('a is not 5 or',b ,'is not greater than zero.')

# ----------------------------
# Be careful using conditionals - correction of code
# Slide 24

day = "Saturday"
temperature = int(input("What is the temperature today?"))
isRaining = input("Is it raining?")
if isRaining == "yes" or "Yes" or "YES":
    raining = True
if isRaining == "no" or "No" or "NO":
    raining = False

# check if the temperature is good and isn't raining
if day =="Saturday" and temperature > 20 and not raining :
    print("Go out ")
else:
    print ("Better finishing python programming exercises")

# ----------------------------
# Repeated Steps
# Slide 25
n = 5
while n > 0:
    print(n)
    n = n - 1
    print('Boom!!')
    print(n)
# What is wrong with this loop?

# ----------------------------
# An Infinite Loop
# Slide 26
n = 5
while n > 0 :
    print('Time')
    print('ticking')
    print('Stopped')
    
# What is wrong with this loop?
""" Ans: It's going to loop forever, because it'll be printing forever without 
changing anything. Something/ some variable needs to change so that the loop
condition evaluates to false and exits the loop """

# ----------------------------
# Another Loop
# Slide 27
# What is wrong with this loop?
n = 0
while n > 0 :
    print('Time')
    print('ticking ')
    print('Stopped')
    
""" Ans: It won't run! The condition is false from the start"""

# ----------------------------
# Breaking Out of a Loop
# Slide 28

while True:
    line = input('> ')
    if line == 'done' :
        break
    print(line)
    print('Done!')

# ----------------------------
# Finishing an Iteration with continue
# Slide 31
while True:
    line = input('> ')
    if line[0] == '#' :
        continue
    if line == 'done' :
        break
    print(line)
    print('Done!')