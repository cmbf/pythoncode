# -*- coding: utf-8 -*-
"""
@author: Cristiano Ferro %(username)
Created on %(date) 
"""
# ----------------------------
# Exercise 6


#msg = 012345678901234
#letters = "abcdefghijklmnopqrstuvwxyz"

msg = "Lisbon is in Portugal"


print(msg[1])             #i
print(msg[0:6])           #Lisbon
print(msg[3:5])           #bo
print(msg[0:9])           #Lisbon is
print(msg[:9])            #Lisbon is
print(msg[10:14])         #in P
print(msg[10:])           #in Portugal
print(msg[:6])            #Lisbon
print(msg[6:])            # is in Portugal
print(msg[:6] + msg[6:])  #Lisbon is in Portugal
print(msg[:])             #Lisbon is in Portugal

# ----------------------------
# Other code
print(msg)
city_name = msg[0:6]
print(city_name)
country_name = msg[13:21]
print(country_name)