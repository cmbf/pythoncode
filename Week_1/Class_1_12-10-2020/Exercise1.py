# -*- coding: utf-8 -*-
"""
@author: Cristiano Ferro %(username)
Created on %(date) 
"""
# ----------------------------
# Outputs commands:

# ----------------------------
# a. Write a code to print out the string “Welcome to python course”

print("Welcome to python course")

print ('Welcome to pythonCourse')


# ----------------------------
# b. Write a code to print out the result of the operations 8+5*13

print(8+5*13)

# ----------------------------
# c. Write a code to print out <Python is fun> <number of
# the days of this month >

print("Python is fun", 31)

# ----------------------------
# Slide 43
# Concatenating strings
# Creating variables to store values
# Splitting lines \n
# and using tabs \t

print("Hello" + "<Your name >")
print("Hello"''"<Your name>")
print("Hello" + "<Your name>\n You are splitting this line several\ntimes\n")
print("Hello, <Your name>")
Greeting = "Hello"
name = "<Your name>" # name = <Your name>   would give an error
print(Greeting + name)
print(Greeting +' '+ name)
print(type(name))
age = 1
print(name + str(age)) # print( name + age)   would give an error




# ----------------------------
# Slide 45
# Strings
# Formatting
# –f-Strings

name = "Josi"
age = 31

print('%s is %d years old' % (name, age))
print('{} is {} years old'.format(name, age))
print(f'{name} is {age} years old')