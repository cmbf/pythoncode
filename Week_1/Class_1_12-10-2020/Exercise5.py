# -*- coding: utf-8 -*-
# ----------------------------
# @author: Cristiano Ferro
# Created on Mon Oct 12 16:10:34 2020
# ----------------------------
# Exercise 5 - Data formatting

money = 1000
ballNr = 3
ballPrice = 450.00

def formatData(balls, money, price):
    letMeTellYouAString = "I have {0} dollars so I can buy {1} football for {2:.2f} dollars."
    print(letMeTellYouAString.format(balls, money, price))
    
# Call function
formatData(ballNr, money, ballPrice)