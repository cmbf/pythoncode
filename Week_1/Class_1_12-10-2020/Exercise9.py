# -*- coding: utf-8 -*-
# ----------------------------
# @author: Cristiano Ferro
# Created on Mon Oct 12 17:05:50 2020
# ----------------------------
# Exercise 9 - Sum of 2 numbers

number1 = int(input("Tell me a number\n"))
number2 = int(input("Tell me another number\n"))
    
# Simple addition function
def addition(num1, num2):
    sumOf2 = num1 + num2
    return sumOf2
    
# Call / print function
print(addition(number1, number2))