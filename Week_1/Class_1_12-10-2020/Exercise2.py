# -*- coding: utf-8 -*-
"""
@author: Cristiano Ferro %(username)
Created on %(date) 
"""
# ----------------------------
# Exercise 2

# ----------------------------
# a. 
print("Student 1\t<Name>")
print("Student 2\t<Name>")

# Other possible solution
print("Student 1\t", input("<Name>"))
print("Student 2\t", input("<Name>"))

# ----------------------------
# b.Display float number with 2 decimal places using print()
print('%.2f' % 458.541315)