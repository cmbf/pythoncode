# -*- coding: utf-8 -*-
# ----------------------------
# @author: Cristiano Ferro
# Created on Mon Oct 12 17:17:45 2020
# ----------------------------
# Exercise 10
# ----------------------------
# Print only the group of numbers "5555" of a sequence
# Extract a selected sequence from a string
# ----------------------------

# Importing module to search patterns
import re

givenStr = 'xxxx----xxxx----5555---'
fives = "5555"

def extractFromStr(text, pattern):
    # Using search() to find pattern and group() to return the part where there was a match
    number5 = re.search(pattern, text).group()
    print(number5)
    
# Call function
extractFromStr(givenStr, fives)

# ----------------------------
# Exercise 10 solution 2
# ----------------------------
credit_str = "xxxx----xxxx----5555---"
new_credit_str = credit_str[credit_str.index('5'):credit_str.index('5') + len('5555')]
print(new_credit_str)

# ----------------------------
# Other code
# ----------------------------
# Slide 65
import sys

print("\ninteget value info: ", sys.int_info)
print("\nmaxsize of an integer: ", sys.maxsize)
