# -*- coding: utf-8 -*-
"""
@author: Cristiano Ferro %(username)
Created on %(date) 
"""
# ----------------------------
# Exercise 7

#msg = 012345678901234
#letters = "abcdefghijklmnopqrstuvwxyz"

msg = "Lisbon is in Portugal" #23 lettters

print(msg[-21:-15]) #Lisbon
print(msg[-8:-5])
print(msg[-14:-12])
print(msg[-8:])
print(msg[-11:-8])
print(msg[-23:])
print(msg[-5:-1])
print(msg[7:])
print(msg[:])