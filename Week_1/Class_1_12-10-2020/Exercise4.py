# -*- coding: utf-8 -*-
# ----------------------------
# @author: Cristiano Ferro
# Created on Mon Oct 12 15:47:49 2020
# ----------------------------
# Exercise 4 - splits name into various lines
# Different from solution since it's a restrictive method that only works when 
# exactly 3 names are given

# Get name
nomeCompleto = input("Please tell me your full name: ")

# Outputs each name into various lines
def nameSplitter(nome):
    # Being friendly (giving feedback to user is important)
    print("\nOh ok! Let me split this for you ;)\n")
    # Splits string into a list of elements
    splitResult = nomeCompleto.split(" ")
    # Prints each element of the new list (each name) in each line
    for i in splitResult:
        print(i)

# Call function
nameSplitter(nomeCompleto)

str1, str2, str3 = input("Enter three string").split()

print(str1, str2, str3)