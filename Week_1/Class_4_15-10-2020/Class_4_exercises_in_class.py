# -*- coding: utf-8 -*-
# ----------------------------
# @author: Cristiano Ferro (utilizador)
# Create on Thu Oct 15 08:53:17 2020
# ----------------------------

# ----------------------------
# Assignment tests
a  = 10
b = a
c = 9
d = c
c = c + 1
s = "wagebuweghsgbjudsw"
n = 5

# ----------------------------
# Countdown
print('\nCountdown')
while n > 0:
    print(n)
    n = n - 1
print('Boom!!')
print(n)

# ----------------------------
# Test of range function
print('\nTest of range function')
for i in range(15, 0, -5):
    print(i)

# ----------------------------
# Find the largest value
print('\nFind the largest value')
largest_so_far = -1
print('Before', largest_so_far)
for the_num in [9,41,12,3,74,15]:
    if the_num > largest_so_far :
        largest_so_far = the_num
    print(largest_so_far, the_num)
print('After', largest_so_far)

# ----------------------------
# Test 2 to find the largest value
print('\nTest 2 to find the largest value')
print('Before')
for value in [9,41,12,3,74,15]:
    if value > 41:
        print('Large number', value)
print('After')

# ----------------------------
# Test 3 to find the largest value
print('\nTest 3 to find the largest value')
found = False
print('Before', found)
for value in [9,41,12,3,74,15]:
    if value == 3 :
        found = True
        print(found, value)
    else:
        found = False
print('After', found)

# ----------------------------
# Test 4 to find the largest value
print('\nTest 4 to find the largest value')
palavra = None
print('Before')
for value in [9,41,12,3,74,15]:
    if palavra is None:
        palavra = value
    elif value < palavra:
        palavra = value
    print(palavra, value)
print('After', palavra)