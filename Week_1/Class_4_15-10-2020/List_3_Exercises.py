# -*- coding: utf-8 -*-
"""
@author: Cristiano Ferro ( %(username) )
Created on %(date) 
"""
# --------------------------------------------------------
# 1 -What do the following expressions evaluate to?
print('\nExercise 1')
print((5 > 4) and (3 == 5))                     # True and False > False
print(not (5 > 4))                              # not True > False
print((5 > 4) or (3 == 5))                      #True or False > True
print(not ((5 > 4) or (3 == 5)))                #not (True or False) > not True > False
print((True and True) and (True == False))      #Tru and False > False
print((not False) or (not True))                # True or False > True


# --------------------------------------------------------
# 2 -Identify the three blocks in this code:
print('\nExercise 2')
spam = 0
if spam == 10:
    print('eggs')           #first block
    if spam > 5:
        print('bacon')      #second block
    else:
        print('ham')        #third block
    print('spam')
print('spam')


# --------------------------------------------------------
# 3 -Write a code that reads n numbers and print how many of them are in the following ranges:
# [0; 25], [26; 50], [51; 75] and [76; 100]. For example, for n = 10 and the following numbers {2;
# 61; -1; 0; 88; 55; 3; 121; 25; 75} , your program should print:
print('\nExercise 3')
aListOfNumbers = [2, 61, -1, 0, 88, 55, 3, 121, 25, 75]

def numberRanges(numberList):
    group1 = 0
    group2 = 0
    group3 = 0
    group4 = 0
    for x in numberList:
        if 0 <= x <= 25:
            group1 += 1
        elif 26 <= x <= 50:
            group2 += 1
        elif 51 <= x <= 75:
            group3 += 1
        elif 76 <= x <= 100:
            group4 += 1
    else:
        print('number not in range of 0 to 100')
    print('[0 ,25]:', group1)
    print('[26 ,50]:', group2)
    print('[51 ,75]:', group3)
    print('[76 ,100]:', group4)

# Run the function by giving a list of numbers as the argument
numberRanges(aListOfNumbers)


# --------------------------------------------------------
# 4 – Use 10 strings methods of your choose to get information from this piece of text
print('\nExercise 4')
stringy = """Catching up
Start with The Portuguese: The Land and Its People (3) by Marion Kaplan (Penguin),
a one-volume introduction ranging from geography and history to wine and poetry,
and Portugal: A Companion History (4) by José H Saraiva (Carcanet Press), a
bestselling writer and popular broadcaster in his own country."""
print("\n10 Methods to get information:")

# Method a - let's go!
# -----------------------------
# how many alphabetic characters appear in the string - using the isalpha method
howManyAlpha = 0
for i in stringy:
    if i.isalpha():
        howManyAlpha += 1
print(f"1 - There\'s {howManyAlpha} alphabetic characters in the string")

# Method 2!
# -----------------------------
# how many alphanumeric characters appear in the string - using isalnum method
howManyAlphanum = 0
for i in stringy:
    if i.isalnum():
        howManyAlphanum += 1
print(f"2 - There\'s {howManyAlphanum} alphanumeric characters in the string")

# Method 3!
# -----------------------------
# check if string is titlecased
if stringy.istitle:
    print("3 - The string is titlecased")
else:
    print("3 - The string isn't titlecased")
    
# Method 4!
# -----------------------------
# check how many lowercase characters are in the string - using islower method
howManyLower = 0
for i in stringy:
    if i.islower():
        howManyLower += 1
print(f"4 - There\'s {howManyLower} lowercase letters in the string")

# Method 5!
# -----------------------------
# how many number characters appear in the string - using isdigit method
howManyNumbers = 0
for i in stringy:
    if i.isdigit():
        howManyNumbers += 1
print(f"5 - There\'s {howManyNumbers} numbers in the string")

# Method 6!
# -----------------------------
# count the number of spaces in a string - using the isspace method
howManySpaces = 0
for i in stringy:
    if i.isspace():
        howManySpaces += 1
print(f"6 - There\'s {howManySpaces} spaces in the string")

# Method 7!
# -----------------------------
# check if the string ends with a certain suffix - using the endswith method
suffix = 'ntry.'
if stringy.endswith(suffix):
    print("7 - String ends with a punctuation sign")
else:
    print("7 - String doesn't seem to end with a punctuation sign")

# Method 8!
# -----------------------------
# check if there's only uppercase characters in the string - using isupper method
howManyUpper = 0
for i in stringy:
    if i.isupper():
        howManyUpper += 1
print(f"8 - There\'s {howManyUpper} uppercase letters in the string")

# Method 9!
# -----------------------------
# how many parentheses appear in the string - using count method
howManyParentheses = stringy.count("(") + stringy.count(")")
print(f"9 - There\'s {howManyParentheses} parentheses in the string")

# Method 10!
# -----------------------------
# check index locations of all parentheses - using the find method
# trying out the pythonic way >>>>>
# result: tried this way, but doesn't work! strings are immutable - UPS
# but here's the non working code commented, because I'm still proud of what I learned with it:

# parenthesesLocations = []
# stringy2 = stringy
# [parenthesesLocations.append(i) for i in stringy]
# while stringy2.find("(") or stringy2.find(")"):
#      parenthesesLocations.append(stringy.find("("))
#      parenthesesLocations.append(stringy.find(")"))
#      stringy2[stringy.find("(")] = ' '
#      stringy2[stringy.find(")")] = ' '
# print(parenthesesLocations)

# the working code - found on stackoverflow
parenthesesLocations = []
index = 0
print("10 - Below are the locations of the parentheses:")
while index < len(stringy):
    if index == stringy.find("(", index):
        index = stringy.find("(", index)
        parenthesesLocations.append(index)
        print(f'found \'(\' at {index}')
    elif index == stringy.find(")", index):
        index = stringy.find(")", index)
        parenthesesLocations.append(index)
        print(f'found \')\' at {index}')
    index += 1
print(f'A simpler list of locations: {parenthesesLocations}')


# --------------------------------------------------------
# 5 – Write a code that reads a number (0-16) in the decimal base and prints that number in the
# binary base.

# Found this code on geeksforgeeks of a function inside a function (inception!!!)
# doesn't seem to print the last digit of the binary number
"""
def DecimalToBinary(num):
    if num > 1:
        DecimalToBinary(num // 2)
        print(num % 2)
DecimalToBinary(2)
"""
# after analising the weird code above I figured out that I can do it without a weird function 
# and was finally able to get the last digit!
decimalNr = 51
listOfBits = []
def binaryGenerator(x, list):
    # Needed to append the first value (actually the last because it's reversed)
    # This way, the last digit will be included instead of being skipped because of the while loop
    list.append(x % 2)
    # Method to cut in half each time, do modulo operation and the remainder will be 0 or 1
    while x > 1:
        x = x // 2
        list.append(x % 2)
    # Numbers come reversed, so doing a reverse here
    list.reverse()
    return list

# Reverse
newList = binaryGenerator(decimalNr,listOfBits)
bitNumberStr = ''
# Convert from list to string
for i in newList:
    # Convert to string so that it doesn't sum the numbers and add to string
    bitNumberStr += str(i)
# Convert back to int since it's a number
bitNumber = int(bitNumberStr)
print(decimalNr, 'as a binary number is:', bitNumber)
