# -*- coding: utf-8 -*-
# ----------------------------
# @author: Cristiano Ferro (utilizador)
# Create on Thu Oct 15 10:02:54 2020
# ----------------------------
# Presentation 3 / Class 4 - Exercise 1

bestProducts = 'The Best of made in Portugal - Hats, Soaps, Shoes, Titles & Ceramics, Cork'
productsSplit = bestProducts.split(' ')
upperLetters = ''
for i in productsSplit:
    if i[0].isupper():
        upperLetters += i[0]
print(upperLetters)