# -*- coding: utf-8 -*-
"""
# @author: Cristiano Ferro
# Created on 14/10/2020
"""
# ----------------------------

# ----------------------------
# 1 - Write a code that reads two numbers and then a character that represents a
# arithmetic operator (‘+’, ‘-’, ‘*’ or ‘/’). Your program should then print the result
# operator applied to the two numbers given.

# read first number
firstNum = int(input("Tell me the first number:"))
# read second number
secondNum = int(input("Tell me the second number:"))

# boolean to control the loop
validOperation = False
# make operation using the variables with the values that were given
while not validOperation:
    # read operator
    operator = input("Tell me which operator, out of the following:\n '+', '-', '*' or '/'\n")
    if operator == "+":
        result = firstNum + secondNum
        validOperation = True
        break
    elif operator == "-":
        result = firstNum - secondNum
        validOperation = True
        break
    elif operator == "*":
        result = firstNum * secondNum
        validOperation = True
        break
    elif operator == "/":
        result = firstNum / secondNum
        validOperation = True
        break
    else:
        print("Value needs ot be one of the following operators instead:\n '+', '-', '*' or '/' ")
        continue

# print result
print("The result of your operation is:",result, '\n')


# 1 - Solution 2 in class
# ----------------------------
n1 = input("\nEnter first number: ")
n2 = input("\nEnter second number: ")
c = input("\nEnter the operator number: ")

str = n1+c+n2
print(f'\n{n1} {c} {n2}')

res = eval(str)
print("\nRES : ", res)

# ----------------------------
# 2 - Consider the following menu: (...)
# Your program should:
# - print the menu;
# - read a number from 1 to 5;
# - and print the option menu corresponding to the number read.
# This must be repeated until the user selects the option 5.

menuOp1 = "1 - Pão Alentejano"
menuOp2 = "2 - Bolo Lêvedo [dos Açores]"
menuOp3 = "3 - Bolo do Caco [da Ilha da Madeira]"
menuOp4 = "4 - Broa"
menuOp5 = "5 – I want to leave"

# boolean to control the loop / option to leave
option5 = False
print("The menu is as follows:\n" + menuOp1 + "\n" + menuOp2 + "\n" + menuOp3 + "\n" + menuOp4 + "\n" + menuOp5 + "\n")
while not option5:
    option = int(input("Please tell me which option you want"))
    # Print item depending on choice
    if option == 1:
        print("You have chosen: " + menuOp1 + "\nBon appetit!")
        continue
    elif option == 2:
        print("You have chosen: " + menuOp2 + "\nBon appetit!")
        continue
    elif option == 3:
        print("You have chosen: " + menuOp3 + "\nBon appetit!")
        continue
    elif option == 4:
        print("You have chosen: " + menuOp4 + "\nBon appetit!")
        continue
    elif option == 5:
        print("You have chosen: " + menuOp5 + "\nBon appetit!")
        option5 = True
        print("Thank you, see you next time.")
        # break

# 3 - What will be printed by the program below? Assume that the value of D in the
# ord(<letter>) of the first letter of your name. Example: ord(A) = 65
D = ord('D')
x = 5 + D
y = 0
while True :
    y = (x % 2) + 10 * y
    x = x // 2
    print ('x =', x, 'y =', y)
    if x == 0:
        break

while y != 0:
    x = y % 100
    y = y // 10
    print ('x =', x, 'y =', y)
    

# 4 – Write a program that reads a positive integer n and prints n lines with the following
# format (example for n = 5 =&gt; 5 spaces)
currentNr = 1
below100 = False
while not below100:
    lastNr = int(input("Give me a number: "))
    if 100 < lastNr:
        print("Please don't be mean to your PC")
        continue
    below100 = True
reachedEnd = False
while not reachedEnd:
    print(' '*(currentNr - 1) + str(currentNr))
    if currentNr == lastNr:
        reachedEnd = True
    currentNr += 1


