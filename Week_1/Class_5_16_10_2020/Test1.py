# sphere volume calculation 

pi = 3.1415
raio = 5.3
print(f"{4/3*pi*(raio**3):.3f} U")
import math

def volume_sphere(radius_1, power, const):
    volume=const*math.pi*(radius_1**power)
    return volume

radius_1 = float(input("Please, Enter radius: "))
const = 1
power = 3
volume = volume_sphere(radius_1, power, 4/3 )
#print(f"{volume:.3f} U")


guess_number = 5
user_number = int(input("Please, Enter a number"))
if user_number < guess_number:
      print('Please, guess higher') 
elif user_number == guess_number :
    print("Congrats, you found the answer")
elif user_number == 9 :
    print("No, it is not 9 ")
else :
    print("Please, guess smaller.")    
print((4 < 5) and (5 < 6))
print((4 < 5) and (9 < 6))
print((1 == 2) or (2 == 2))
print((2 + 2 == 4 and not 2 + 2 == 5) and 2 * 2 == 2 + 2)

user_age = int(input("Please, Enter you Age"))

minimum_age  = 18
retire_age =67
if user_age <= 18:
    print("You are too young to work, come back to school")
elif user_age > 18 and (user_age < 67 and user_age < 200 ):
    print ("Have a good day at work")
else :    
    print("You have worked enough, Let’s Travel now ")


while True:
    line = input('> ')
    if line[0] == '#' :
        continue
    if line == 'the end of our class' :
        break
    print(line)
print('Done!')

print('Before')
for i in range(5):
    print(i)
print('After')

print('Before')
for i in range(15, 0, -5):
    print(i)
print('After')

a = 10 
b = a 
c = 9
d = c
c = c + 1
s = "Joseanne"
n = 5
while n > 0 :
    print(n)
    n = n - 1
print('Boom!!')
print(n)


for student_age in [5, 4, 3, 2, 1, 0] :
    print(student_age)
print('Boom!!')

friends = ['Ana', 'Filipe', 'João', 'Miguel']
for friend in friends : 
    print('Congrats on your new job:', friend)
print('Done!')

print('Before')
for i in range(7):
    print(i)
print('After')


print('Before')
for i in range(1000, 50000):
      print(i)
print('After')

print('Before')
for i in range(15, 0, -5):
    print(i)
print('After')

print('Before')
for thing in [9, 41, 12, 3, 74, 15] :
      print(thing)
print('After')


msg = "joseanne is a good teacher "
a= msg.capitalize()
number_of_as = msg.count("a")
print(number_of_as)
print(a)


loop_interaction = 0
print('Before', loop_interaction )
for thing in [9, 41, 12, 3, 74, 15] :
    loop_interaction = loop_interaction + 1
    # print(loop_interaction , thing)
print('After', loop_interaction )


palavra = None
print('Before')
for value in [9, 41, 12, 3, 74, 15] :
    if palavra is None : 
        palavra = value
    elif value < palavra : 
        palavra = value
    print(palavra, value)
print('After', palavra)

a = 10           # 10 =  1010 
b = 3            # 3 =   0011 

hifen =30
binary = bin(a & b);        # 2 =  0010
denary = a & b
print ("Line 1 - Value of binary is ", binary)
print ("Line 1 - Value of denary is ", denary)
print("-"*hifen)


binary = bin(a | b);        # 61 = 0011 1101
denary = (a | b)
print ("Line 1 - Value of binary is ", binary)
print ("Line 1 - Value of denary is ", denary)
print("-"*hifen)


binary = bin(a << 2);        # 2 =  0010
denary = a << 2;
print ("Line 1 - Value of binary is ", binary)
print ("Line 1 - Value of denary is ", denary)
print("-"*hifen)


binary = bin(a >> 2);        # 2 =  0010
denary = a >> 2;
print ("Line 1 - Value of binary is ", binary)
print ("Line 1 - Value of denary is ", denary)
print("-"*hifen)


binary = bin(~a);        # 2 =  0010
denary =~a;
print ("Line 1 - Value of binary is ", binary)
print ("Line 1 - Value of denary is ", denary)
print("-"*hifen)


binary = bin(a ^ b);        # 2 =  0010
denary =a ^ b;
print ("Line 1 - Value of binary is ", binary)
print ("Line 1 - Value of denary is ", denary)
print("-"*hifen)

while True:
    print('Hello world')
    break

# ----------------------------
    
def food(vegetable):
      if vegetable == 'tomato':
          print('I bought tomato')
      elif vegetable == 'orange':
          print('I bought orange')
      else:
          print('I bought other vegetable')
 
food('banana')


def addtwo(a, b):
    added = a + b
    subtracao = a - b
    return added, subtracao

x = addtwo(3, 5)
print(x[0])
print(x[1])


def food():
    eggs = 'food local'
    print(eggs) # prints ‘food local'
    return eggs
   
def more_food():
    eggs = 'more_food local'
    print(eggs) # prints ‘more_food local’
    food_1 = food()
    print(eggs) # prints ‘more_food local'
    print(food_1)
   

eggs = 'global'
more_food()
print(eggs) # prints 'global'


a = 3
b = 10 
c = 0

c = a + b
c += a # c + a
c *= a # c * a
c /= a # c / a float 
c = 4 
c %= a # c % a

c **= a # c** a
c //= a # c/ a  int

















