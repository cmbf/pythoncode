# -*- coding: utf-8 -*-
# ----------------------------
# @author: Cristiano Ferro (utilizador)
# Create on Fri Oct 16 11:13:48 2020
# ----------------------------
# Implement a function that receives a number a parameter and prints, in
# decreasing order, which numbers are even and which are odd, until it reaches 0,

def even_odd(num):
    for i in range(num):
        if num %2 == 0:
            print('Even number', num)
        else:
            print('Odd number', num)
        num -=1

even_odd(15)