# -*- coding: utf-8 -*-
# ----------------------------
# @author: Cristiano Ferro (utilizador)
# Create on Fri Oct 16 11:32:26 2020
# ----------------------------

# 
listOfNrs = [2, 61, -1, 0, 88, 55, 3, 121, 25, 75]

def avgNr(listNr):
    total = 0
    mean = 0
    for i in listNr:
        total += i
    mean = total / len(listNr)
    return mean

mean = avgNr(listOfNrs)
print(mean)