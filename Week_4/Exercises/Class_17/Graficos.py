# -*- coding: utf-8 -*-
"""
@author: Cristiano Ferro %(username)
Created on %(date) 
"""
# ----------------------------

# Gráficos com dados do PISA
# ----------------------------

import numpy as np
import matplotlib.pyplot as plt
from collections.abc import Iterable
graph_path = "C:\\Users\\criso\\OneDrive\\Documentos\\Repos\\pythoncode\\Week_4\\Exercises\\Class_17\\DP_LIVE_04112020161547500.csv"

def countryClassif(country_initials):

    girls_values = []
    girls_years = []
    boys_values = []
    boys_years = []
    with open(graph_path) as csv_file:
        # Find indexes of year, classifications, country and sex with header 
        header = csv_file.readline().replace('"', '').strip("ï»¿").split(",")
        time_index = header.index("TIME")
        value_index = header.index("Value")
        location_index = header.index("LOCATION")
        sex_index = header.index("SUBJECT")
        boys = []
        for line in csv_file:
            string_data = line.replace('"', '').strip("ï»¿").split(",")[0:-1]
            if string_data[0] == country_initials:
                # Retorna lista de classificações ao longo do ano
                # De meninas
                if string_data[sex_index] == "GIRL":
                    
                    girls_values.append(float(string_data[value_index]))
                    girls_years.append(int(string_data[time_index]))
                elif string_data[sex_index] == "BOY":
                    boys_values.append(float(string_data[value_index]))
                    boys_years.append(int(string_data[time_index]))
                else:
                    pass
            else:
                pass
        
        return girls_values, girls_years, boys_values, boys_years

def graficoAno(country, girls_val, boys_val, girls_yr, boys_yr, ano):
    
    # Find index of year 2018
    girls_index_ano = girls_yr.index(int(ano))
    boys_index_ano = boys_yr.index(int(ano))
    
    # Find lowest and highest values so that plots show better informations
    graph_min = min(min(boys_val), min(girls_val))
    graph_max = max(max(boys_val), max(girls_val))
    
    # Boys plot
    x = np.array(["Girls", "Boys"])
    y = np.array([girls_val[girls_index_ano], boys_val[boys_index_ano]])
    plt.bar(x,y, color ="#b0eeff", edgecolor="#95b6e8", label='Portugal', linewidth=4)
    plt.bar(x,y, color ="#b0eeff", edgecolor="#95b6e8", label='Portugal', linewidth=4)
    plt.axis(xmin=-1, xmax=2, ymin=graph_min-10, ymax=graph_max+10)
    plt.legend(["Girls", "Boys"], loc='upper right')
    plt.title(f"Mathematics performance in {country} by genre, Year {ano}")
    plt.show()

def valoresPaises(country_list, girls_val, boys_val, girls_yr, boys_yr):
    total_means_min = []
    total_means_max = []
    
    if isinstance(country_list, list):
        for i in range(len(country_list)):
            # Combine boys and girls by getting the mean
            mean_values = [(a + b)/ 2 for a, b in zip(girls_val[i], boys_val[i])]
            # Find lowest and highest values so that plots show better informations
            graph_min = min(mean_values)
            graph_max = max(mean_values)
            # append to list for following countries
            total_means_min.append(graph_min)
            total_means_max.append(graph_max)
            # Boys plot
            x = np.array(girls_yr[i])
            y = np.array(mean_values)
            plt.plot(x, y, marker=".")

            
    # Same version of the previous
    else: 
        mean_values = [(a + b)/ 2 for a, b in zip(girls_val, boys_val)]
        final_min = min(mean_values)
        final_max = max(mean_values)
        # values = sum(mean_values) / len(mean_values)
        # Boys plot
        x = np.array(girls_yr)
        y = np.array(mean_values)
        plt.plot(x, y, marker=".")
    if isinstance(country_list, list):
        plt.legend(tuple(country_list), loc='upper right')
        final_min = min(total_means_min)
        final_max = max(total_means_min)
        plt.title(f"Mathematics performance in 5 countries")
    else:
        plt.legend([country_list], loc='upper right')
        plt.title(f"Mathematics performance in {country_list}, Year {boys_yr}")
    plt.axis(ymin=final_min-20, ymax=final_max+20)
    plt.show()



# Gráfico de médias obtidas por ano de portugal (PRT)
# ----------------------------

pt_girls_values, pt_girls_years, pt_boys_values, pt_boys_years = countryClassif("PRT")

valoresPaises("PRT", pt_girls_values, pt_boys_values, pt_girls_years, pt_boys_years)


# Para outros anos
# ----------------------------
# graficoAno("CAN", can_girls_values, can_boys_values, can_girls_years, can_boys_years, ano)
# graficoAno("USA", usa_girls_values, usa_boys_values, usa_girls_years, usa_boys_years, ano)
# graficoAno("BRA", bra_girls_values, bra_boys_values, bra_girls_years, bra_boys_years, ano)
# graficoAno("FRA", fra_girls_values, fra_boys_values, fra_girls_years, fra_boys_years, ano)
# graficoAno("JPN", jpn_girls_values, jpn_boys_values, jpn_girls_years, jpn_boys_years, ano)


# ----------------------------
# outro de 5 outros os países(livre escolha) comparados com Portugal

countries_initials = ["CAN", "USA", "BRA", "FRA", "JPN"]

can_girls_values, can_girls_years, can_boys_values, can_boys_years = countryClassif("CAN")
usa_girls_values, usa_girls_years, usa_boys_values, usa_boys_years = countryClassif("USA")
bra_girls_values, bra_girls_years, bra_boys_values, bra_boys_years = countryClassif("BRA")
fra_girls_values, fra_girls_years, fra_boys_values, fra_boys_years = countryClassif("FRA")
jpn_girls_values, jpn_girls_years, jpn_boys_values, jpn_boys_years = countryClassif("JPN")

girl_values = [can_girls_values, usa_girls_values, bra_girls_values, fra_girls_values, jpn_girls_values]
girls_years = [can_girls_years, usa_girls_years, bra_girls_years, fra_girls_years, jpn_girls_years]
boys_values = [can_boys_values, usa_boys_values, bra_boys_values, fra_boys_values, jpn_boys_values]
boys_years = [can_boys_years, usa_boys_years, bra_boys_years, fra_boys_years, jpn_boys_years]

valoresPaises(countries_initials, girl_values, boys_values, girls_years, boys_years)

# ----------------------------
# ----------------------------
# ----------------------------
# gráfico de barras com os dados em relação ao desempenho de meninos e as meninas

graficoAno("PRT", pt_girls_values, pt_boys_values, pt_girls_years, pt_boys_years, 2018)

