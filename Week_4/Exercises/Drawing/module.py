# -*- coding: utf-8 -*-
"""
@author: Cristiano Ferro %(username)
Created on %(date) 
"""
# ----------------------------

import GeometryCalculations
import math

# Circle Perimeter
def circlePerimeter(r):
    return 2*math.pi*r

# Circle Area
def circleArea(r):
    return math.pi*(r**2)

# Sphere Volume
def sphereVolume(r):
    sphere_volume = (4/3) * math.pi * r**3
    return sphere_volume

# Square area
def squareArea(side):
    return GeometryCalculations.squareArea(side)


# Square perimeter given side
def squarePerimeter(side):
    return side*4
    

radius = float(input("Radius of circle: "))
side = float(input("Side of square: "))

circleArea(radius)
squareArea(side)
circlePerimeter(radius)
squarePerimeter(side)
print(f'Circle:\nArea: {round(circleArea(radius), 2)} Perimeter {round(circlePerimeter(radius), 2)}\n')
print(f'Square:\nArea: {squareArea(side)} Perimeter {squarePerimeter(side)}')