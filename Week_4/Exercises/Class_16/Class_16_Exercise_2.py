# -*- coding: utf-8 -*-
"""
@author: Cristiano Ferro %(username)
Created on %(date) 
"""
# ----------------------------
from functools import reduce

int_list = [1, 4, 5, 1, 6, 3, 2, 1, 2, 9, 1, 4, 6, 3, 9, 7]

def frequencies(v):
    lowest = len(v)
    highest = 0
    f1 = []
    f2 = []
    dict_freqs = {}
    for i in v:
        dict_freqs.setdefault(i, 0)
        dict_freqs[i] += 1
    print(dict_freqs)
    for v in dict_freqs.values():
        if v < lowest:
            lowest = v
        elif v > highest:
            highest = v
        else:
            pass
    for k, v in dict_freqs.items():
        print(k)
        if v == lowest:
            f1.append(k)
        elif v > highest:
            f2.append(k)
        else:
            pass
    return (f1, f2)

print(frequencies(int_list))