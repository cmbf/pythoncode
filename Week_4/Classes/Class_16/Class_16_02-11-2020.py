# -*- coding: utf-8 -*-
# ----------------------------
# @author: Cristiano Ferro (utilizador)
# Create on Mon Nov  2 09:34:28 2020
# ----------------------------

# List 2 exercise 1 update:
# Solution 2 in class
# ----------------------------
n1 = input("\nEnter first number: ")
n2 = input("\nEnter second number: ")
c = input("\nEnter the operator number: ")

str = n1+c+n2
print(f'\n{n1} {c} {n2}')

res = eval(str)
print("\nRES : ", res)

day = "Saturday"
temperature = 30
raining = True

if day == "Saturday" and temperature > 20 and not raining:
    print("Go out")
else:
    print("Better finishing python programming exercises")


# ----------------------------
# Review of week 2
# ----------------------------
# import sqlite3
from sqlite3 import Error
# from numpy import * # not a good practice
import numpy as np 

b = [4, 6, 7]
print(np.add(b, 5))

print(dir(np))

L = [3, True, 'all', 2.7, [5,8]]
a = [3, [109, 27], 4, 15]
# print(a(1))
print(a[1]) # [109, 27]

a = [7, 5 , 30, 2, 6, 25]
print(a[1:4]) # [5, 30, 2]

a = []
for i in range(4):
    a.append(i)
print(a) # [0, 1, 2, 3]

a = [1, 3, 6, 5, 3]
print(a.count(3)) # 2


# ----------------------------
# Tuple

t = ('English', 'History', 'Mathematics')
print(t[1]) # History
print(t.index('English')) # 0

t = (1, 9, 3, 9)
print(t.count(9)) # 2
print(max(t)) # 9
print(t + t) # (1, 9, 3, 9, 1, 9, 3, 9)

print((1,2) == (2,1)) # False


t = (4, 7, 2, 9, 8)
x = list(i)
x.remove(2)
t = tuple(x)
rpint(t)
(res)

print(x[0])
(1, 3)

z = ((1, 3), (2, 4))
u = zip(*z)
print(list(u))
[(1,2), (3, 4)]

a = (1, 2)
b = (3, 4)
c = zip(a, b)
x = list(c)
print(x)

# ----------------------------
# Dictionaries


d = {'brand': 'cherry', 'model': 'arizo5', 'color': 'white'}
d['color'] = 'black'
print(d) # {'brand': 'cherry', 'model': 'arizo5', 'color': 'black'}

x = d.get("model")
print(x) # arizo5
print(list(d.keys())) # ['brand', 'model', 'color']

print(list(d.values())) # ['cherry', 'arizo5', 'black']

d.pop('model') # 'arizo5'
print(d) # {'brand': 'cherry', 'color': 'black'}

num = {'all': [12, 13, 8], 'sara': [15, 7, 14], 'taha': [5, 18, 13]}
d = {k: sorted(v) for k, v in num.items()}
print(d) # {'all': [8, 12, 13], 'sara': [7, 14, 15], 'taha': [5, 13, 18]}

k = ['red', 'green']
v = ['#', '##']
z = zip(k, v)
d = dict(z)
print(d) # 

# ----------------------------
# Sets
f = {'apple', 'orange', 'banana'}
f.add('cherry')


# ----------------------------
# Arquivos

encoding_type = encoding="utf-8"

epopeia = open('C:\\Users\\utilizador\\Desktop\\pythoncode\\Week_4\\Classes\\Epopeia.txt', encoding=encoding_type)
for line in epopeia:
    print(line)
    if 'tempestade' in line.lower():
        print(line)

epopeia.close()

with open('C:\\Users\\utilizador\\Desktop\\pythoncode\\Week_4\\Classes\\Epopeia.txt', encoding=encoding_type) as epopeia:
    for line in epopeia:
        if 'tempestade' in line.lower():
            print(line)
print(line)

file_path = 'C:\\Users\\utilizador\\Desktop\\pythoncode\\Week_4\\Classes\\Epopeia.txt'
with open(file_path, 'r', encoding=encoding_type) as epopeia:
    line = epopeia.readline()
    while line:
        print(line, end='')
        line = epopeia.readline()
        
file_path = 'C:\\Users\\utilizador\\Desktop\\pythoncode\\Week_4\\Classes\\Epopeia.txt'
with open(file_path, 'r', encoding=encoding_type) as epopeia:
    line = epopeia.readline()
    while line:
        poem = epopeia.readlines()
        print(type(poem))
        
file_path = 'C:\\Users\\utilizador\\Desktop\\pythoncode\\Week_4\\Classes\\Epopeia.txt'
with open(file_path, 'r', encoding=encoding_type) as epopeia:
    text = epopeia.read()
    print(type(text))

# ----------------------------
# JSON
import json

with open('C:\\Users\\utilizador\\Desktop\\pythoncode\\Week_4\\Classes\\data.JSON') as json_file:
    jsonOBJ = json.load(json_file)
    name = jsonOBJ['data'][0]['name ']
    id_ = jsonOBJ['data'][0]['id']
    address = jsonOBJ['data'][0]['address']['city']
    state = jsonOBJ['data'][0]['address']['state ']
    postal_code = jsonOBJ['data'][0]['address']['postal_code']
    name1 = jsonOBJ['data'][1]['name ']
    id_1 = jsonOBJ['data'][1]['id']
    address1 = jsonOBJ['data'][1]['address']['city']
    state1 = jsonOBJ['data'][1]['address']['state ']
    postal_code1 = jsonOBJ['data'][1]['address']['postal_code']
    name2 = jsonOBJ['data'][2]['name ']
    id_2 = jsonOBJ['data'][2]['id']
    address2 = jsonOBJ['data'][2]['address']['city']
    state2 = jsonOBJ['data'][2]['address']['state ']
    postal_code2 = jsonOBJ['data'][2]['address']['postal_code']
    
    # print(jsonOBJ)
    print(name)
    print(address)
    print(id_)
    print(state)
    print(postal_code)
    print(name1)
    print(address1)
    print(id_1)
    print(state1)
    print(postal_code1)
    print(name2)
    print(address2)
    print(id_2)
    print(state2)
    print(postal_code2)
    a = json.dumps(jsonOBJ, indent=4)
    # print(a)

# INC

# import pandas as pd
# a = 'C:\\Users\\utilizador\\Desktop\\pythoncode\\Week_4\\Classes\\readJson.py'
# all_data = pd.read_excel(a, index_col=1)
# email = all_data['email'].head()
# company_name = all_data['company_name']
# xl = pd.ExelFile(a)
# df = xl.parse('b')

# From Slide 22
# ----------------------------
def power_tree(x):
    return(x**3)
print(power_tree(10))

a = lambda x : x**3
print(a(10))

def larger_num(num1, num2):
    if num1 > num2:
        return num1
    else:
        return num2
print(larger_num(5, 7))

larger_num = lambda num1, num2: num1 if num1 > num2 else num2
print(larger_num(5, 7))

text = "LISBON IS IN PORTUGAL"

def char_lowercase():
    char_lowercase = [char.lower() for char in text]
    return char_lowercase

def map_lowercase():
    map_lowercase = list(map(str.lower, text))
    return map_lowercase

def comp_words():
    words_lowercase = [word.lower() for word in text.split(' ')]
    return words_lowercase

def map_words():
    map_w = list(map(str.lower, text.split(' ')))
    return map_w

print(char_lowercase())
print(map_lowercase())
print(comp_words())
print(map_words())

vegetables = [
    ["Beets", "Cauliflower", "Broccoli"],
    ["Beets", "Carrot", "Cauliflower"],
    ["Beets", "Carrot"],
    ["Beets", "Broccoli", "Carrot"],
    ]

# return only lists that don't have the element "Broccoli"
def not_broccoli(food_list: list):
    return "Broccoli" not in food_list

broccoli_less_list = list(filter(not_broccoli, vegetables))

print(broccoli_less_list)


class josi:
    a = [2, 4, 6]
    b = [7, 8]
    c = [10, 11, 21]
    def __init__(self, val):
        self.val = val
        print(val)
    def increase(self):
        self.val += 1
    def show(self):
        print(self.val)
        
x = josi(3)
c = x.increase()
d = x.show()
print(type(x.a))
print(x.a.__add__(x.b))
print(x.a.__eq__(x.b))
print(x.a.__ne__(x.c))






