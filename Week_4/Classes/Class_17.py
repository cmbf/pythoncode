# -*- coding: utf-8 -*-
# ----------------------------
# @author: Cristiano Ferro (utilizador)
# Create on Wed Nov  4 09:22:24 2020
# ----------------------------

import numpy as np

print(np.__version__)
print(np.version)

students = np.array([[[1, 3, 5], [1, 1, 1]],
                    [[4.5, 4, 5], [4.3, 4.4, 4.6]]], dtype=object)
print(students.ndim, students.dtype)
print(students.shape)

students1 = np.array([[1, 3, 5], [1, 3, 5]])
print(students1.shape)

print(students)
print(students.shape, "\n",
      students.nbytes, "\n",
      students.ndim, "\n",
      students.dtype, "\n",
      students.size, "\n",
      students.data, "\n",
      students.itemsize, "\n")

x1 = np.array([[-1, 3]])
x2 = np.array([[1, 2]])
x3 = x1 + x2
x4 = x1 * x2
x5 = x1 - x2
print(x1)
print(x2)
print(x3)
print(x4)
print(x5)

# Slide 8
# ----------------------------

x3 = np.power(10, 4, dtype=np.int8)
print(x3)
x4 = np.power(10, 4, dtype=np.int32)
print(x4)

# Slide 10
# ----------------------------
print(np.arange(10))

# Reshape mantém o mesmo tamanho
print(np.arange(10).reshape(2, 5))

# Resize para modificar e acrescentar outros valores
print((np.resize(np.arange(10),(2,7))))
print((np.resize(np.arange(30),(3,20))))


# Slide 11
# ----------------------------
d = np.arange(2,5)

print(np.arange(2,5))
print(np.shape)
print(d[:, np.newaxis]) # Adds a column / dimension
print(d[:, np.newaxis].shape)
x = np.array([1, 4, 3])
y = x
z = np.copy(x)
x[1] = 2
print(x)
print(y)
print(z)

# Slide 12
# ----------------------------
dtype = [('name', 'S10'), ('grade', float), ('age', int)]
values_arr = [('Cris', 5, 23), ('Cris Clone', 5, 20),
              ('Cris Clone 2', 5, 19)]
sorted_data = np.array(values_arr, dtype=[('name', 'U12'), ('grade', '<f8'), ('age', '<i4')])
print(np.sort(sorted_data, order='age'))

# Slide 13
# ----------------------------
print(np.zeros((3)))
print(np.zeros((3, 4)))
b = np.zeros((10, 6), dtype=float)
print(b)
print(b.ndim, b.dtype, b.size)
b = np.zeros((10,10))
print("%d bytes" % (b.size * b.itemsize))


b = np.zeros((1000, 1000, 1), dtype=float)
print("%d bytes" % (b.size * b.itemsize))

b = np.zeros(10)
b[2] = 1
b = np.arange(10,50)
print(b)
print(b.ndim, b.dtype, b.size)

print(np.ones((1000,1000))) # Needs to be a tuple!
b = np.arange(50)
print(b)
b = b[::-1]
print(b)
b = b.reshape(5,10)
print(b)
print(b[4, 1])
print(b[3,:])
print(b[0,2:])
print(b[:1,1:])
c = np.ones((2, 10))
print(c)
print(np.concatenate((b,c)))

a = np.full((2,1), np.inf)
print(np.full((2,1), np.inf))
print(a)
print(a.dtype, (a.size * a.itemsize))
print(np.empty((2,1),))

a = np.arange(2, 6)
b = np.array([[3, 5], [4, 6]])
print(np.vstack(a))
print(np.hstack(b))

d = np.ones((1, 3))
x = np.zeros_like((d))
x1 = np.ones_like((x))
print(d)
print(x)
print(x1)

b = np.eye(3)
print(b)

print(np.empty((1, 2)))
print(np.full((2, 2), np.inf))

b = np.arange(50)
c = b.astype(np.int16)
print(b.astype(np.int16))
print(b.ndim, b.dtype, b.size)
print(c.ndim, c.dtype, c.size)

b = np.random.random((10, 10))
bmin, bmax = b.min(), b.max()
print(bmin, bmax)

b = np.random.random((3,3,3))
print(b)



yesterday = np.datetime64('today') - np.timedelta64(1)
today = np.datetime64('today')
tomorrow = np.datetime64('today') + np.timedelta64(1)
print(yesterday, today, tomorrow)

b = np.arange('2019', '2021', dtype='datetime64[D]')
print(b.size, b.ndim)

print(np.ones((2, 6))+np.ones(2, 6))

x1 = np.array([[-1, 3], [4, 2]])
x2 = np.array([[1, 2,], [3, 4]])
x4 = x1+x2
x5 = x1*x2
x3 = np.dot(x1, x2)
print(x3)
print(x4)
print(x5)

import matplotlib.pyplot as plt

values = np.linspace(-(2*np.pi),2*np.pi, 100) # 2*np.pi == 6.28 - portanto o valor mais alto do gráfico, 100 é quantas vezes divide / quantos pontos vai mostrar
print(np.linspace(-(2*np.pi),2*np.pi, 20))
cos_values = np.cos(values)
sin_values = np.sin(values)

plt.plot(cos_values, color = 'blue', marker='*')
plt.plot(sin_values, color = "red", marker='*')
plt.show()

from PIL import Image

my_path = "C:\\Users\\criso\\OneDrive\\Documentos\\Repos\\pythoncode\\Week_4\\Classes\\coquito.png"

im3 = Image.open(my_path)
im_p3 = np.array(im3.convert('L'))
plt.imshow(im_p3)