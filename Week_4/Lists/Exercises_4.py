# -*- coding: utf-8 -*-
"""
@author: Cristiano Ferro %(username)
Created on %(date) 
"""
# ----------------------------
import numpy as np

sudoku_matrix = np.array([
    [4, 2, 6, 5, 7, 1, 3, 9, 8],
    [8, 5, 7, 2, 9, 3, 1, 4, 6],
    [1, 3, 9, 4, 6, 8, 2, 7, 5],
    [9, 7, 1, 3, 8, 5, 6, 2, 4],
    [5, 4, 3, 7, 2, 6, 8, 1, 9],
    [6, 8, 2, 1, 4, 9, 7, 5, 3],
    [7, 9, 4, 6, 3, 2, 5, 8, 1],
    [2, 6, 5, 8, 1, 4, 9, 3, 7],
    [3, 1, 8, 9, 5, 7, 4, 6, 2]
    ])

def isSolution(mat):
    # Check if there're repeated numbers in each row, column
    rep_num = np.any(np.diff(np.sort(mat, axis=0), axis=0) == 0)
    # Check if the sum of all numbers amounts to 405 (or 45 (sum of 1 to 9) * 9 rows)    
    sum_matrix = np.sum(mat)
    correct_matrix = (sum_matrix == 405)
    
    return not rep_num and correct_matrix

isSolution(sudoku_matrix)


# Exercise 3

list_example = [1, 2, 3, 7, 11, 13]

A = 21

def listReader(list_in, A):
    saved_list = list_in
    for i in saved_list:
        for j in saved_list:
            if i*j == A:
                return f'{i} and {j}'
            
print(listReader(list_example, A))