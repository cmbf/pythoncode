# -*- coding: utf-8 -*-
"""
@author: Cristiano Ferro %(username)
Created on %(date) 
"""
# ----------------------------

# Exercise 1 
# ----------------------------
# Qual o valor de cada expressão?
print("1.")
# ----------------------------
# a. map(lambda x:x+1, range(1,4))
print("a.")
print("Ans.: Returns +1 for each value between 1 and 4 (not included)")
# ----------------------------
# b. map(lambda x:x>0, [3,−5,−2,0])
print("\nb.")
print("Ans.: Returns True if bigger than 0 or False if not for each element of the list")

# ----------------------------
# c. filter(lambda x:x>5, range(1,7))
print("\nc.")
print("Ans.: Returns the elements that are bigger than 5 - in this case would be only the number 7")

# ----------------------------
# d. filter(lambda x:x%2==0, range(1,11))
print("\nd.")
print("Ans.: Returns even numbers in a range between 1 and 11 (not included)")

# ----------------------------
# Exercise 2
print("2.")
# Determine o valor de cada uma das expressões seguintes:

# ----------------------------
# a. reduce(lambda y, z: y* 3+z, range(1,5))
print("a.")
print("Ans.: Multiplies y by each element(+3) of the iterable, saving the result in y each time and multiplies again by z (the next element(+3) of the iterable) until reaching the end of the iterable")

# ----------------------------
# b. reduce (lambda x, y: x** 2+y, range(2,6))
print("\nb.")
print("Ans.: Calculates x to the power of each element(+2) of the iterable, saving the result in x each time and calculates x to the power of y (the next element(+2) of the iterable) again until reaching the end of the iterable")

