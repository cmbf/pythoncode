# -*- coding: utf-8 -*-
# ----------------------------
# @author: Cristiano Ferro (utilizador)
# Create on Wed Oct 28 08:50:57 2020
# ----------------------------
def multiplica_numero(numero, valor=2):
    return numero * valor

multiplica_numero(2) # 4
print(multiplica_numero(2, 3)) # 6

def aplica_funcao_a_elementos(lista, funcao):
    for i in range(len(lista)):
        lista[i] = funcao(lista[i])
    return lista

# aplica_funcao_a_elementos([2, 5, 1, 8], add_two)


def multiply(a,b):
    return a*b

multiplied_list = list(map(multiply, (1,2,3), (2,3,4)))
print(multiplied_list)

# map
# número de argumentos da função no map tem que ser igual ao número de listas usado no map

a = [1, 22, 33, 2, 16]
def lt_dezasseis(x):
    return x < 16

# filter retorna os elementos da lista para os quais a função não dá False
filtered_a = list(filter(lt_dezasseis, a))
print(filtered_a)

a = ['abc', 'fbjlf', 'nfgjkls']
sorted_a = sorted(a,key=len)
print(sorted_a)

sorted_a = sorted(a, key=len, reverse=True)
print(sorted_a)

a = ['abc', 'ghi', 'def']

def my_sum(lista):
    accumulator = type(lista[0])()
    for item in lista:
        accumulator += item
    return accumulator

sumed_a = my_sum(a)

print(sumed_a)

lista_listas = [[1, 2], [3, 4], [5, 6]]
sumed_lista_listas = my_sum(lista_listas)
print(sumed_lista_listas)
def add(a,b):
    return a + b
from functools import reduce
sumed_lista_listas =  reduce(add, lista_listas)
print(sumed_lista_listas)

fruits = ['banana', 'apple', 'peach']
sumed_fruits = reduce(add, fruits, 'I love ')
print(sumed_fruits)
sumed_fruits = reduce(lambda x,y: x + y, fruits) # lambda args statements
print(sumed_fruits)


[x + x for x in [1, 2, 3]]
reset = []
for item in [1, 2, 3]:
    reset.append(item)

a = [[1,2], [3,4]]
flattened_a = [sub_item for item in a for sub_item in item]

#outra forma de fazer o flattening

flattened_a = reduce(lambda x,y: x + y, a)
print(flattened_a)

2//4


#Variável global e local

a = 4
def printFuncao():
    a = 17
    print("Dentro da funcao, ", a)
printFuncao()
print("Fora da funcao: ", a)


a = 0
def funxn():
    a = 2
    print('local', a)
funxn()
print(a)

def function():
    global a
    a = 2
    print('local:', a)

# non local keyword
def funcxn1():
    nonlocal a
    a = 3
    print('non-local:', a)
    

if 6 > 3\
    and 6 > 1:
    print("código que continua noutra linha")
else:
    pass
