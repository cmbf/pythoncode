# -*- coding: utf-8 -*-
"""
@author: Cristiano Ferro %(username)
Created on %(date) 
"""
# ----------------------------
# Ficha 2
# ----------------------------

# ----------------------------
# 1. a.
print("1. a. Ans.: Divide um número por outro e previne o erro de divisão por 0. Caso o divisor seja 0, imprime uma mensagem")

# ----------------------------
# b.
print("1. b. Ans.: \n 2 \n 0 \n Imprime 'Divisao por zero' \n Imprime o docstring da função se existir \n Erro - os 2 argumentos não são opcionais")


# ----------------------------
# c.

def divisao_inteira(x, y):
    """
    retorna o resultado de uma divisão por 2

    Parameters
    ----------
    x : int
        dividendo.
    y : int
        divisor.

    Returns
    -------
    int
    resultado da divisão

    """
    return x // y


# ----------------------------
# 2. a.
print("2. a. Ans.: Resultado de 3 elevado a 2 mas não é impresso nem guardado em lado nenhum")
print("2. a. Ans.: Resultado de 2 elevado a 3  mas não é impresso nem guardado em lado nenhum")
print("2. a. Ans.: Imprime resultado de 3 elevado a 2")
print("2. a. Ans.: Imprime resultado de 3 elevado a 2")
print("2. a. Ans.: Imprime resultado de 2 elebado a 0")
print("2. a. Ans.: Erro - os 2 argumentos não são opcionais")

def potencia(a, b):
    return a**b
a = 2
b = 3
potencia(b, a)
potencia(a, b)
print(potencia(b, a))
print(potencia(a, b))
print(potencia(2,0))
print(potencia(2))

# ----------------------------
# b.

def potenciaP(a):
    return a**a

# ----------------------------
# c.

def potencia1(a, b=None):
    if b == None:
        return a**a
    else:
        return a**b
    
print(potencia1(2,0))

def potencia2(a, b=a):
    return a**b

potencia2(0)


# ----------------------------
# Exercicio 3
# a.
print("3. a. Ans.: A variável a quando impressa dentro da função retorna o valor local, fora retorna o global")

# ----------------------------
# b.
print("3. b. Ans.: Variável local só a acessível dentro de uma função, global tanto fora como dentro")

a = 4
def printFuncao():
    a = 17
    print("Dentro da função, ", a)
printFuncao()
print("Fora da função: ", a)

a = 0
def funxn():
    a = 2
    print('local', a)
funxn()
print(a)

def function():
    global a
    a = 2
    print('local:', a)

# non local keyword
def funcxn1():
    nonlocal a
    a = 3
    print('non-local:', a)
    

if 6 > 3\
    and 6 > 1:
    print("código que continua noutra linha")
else:
    pass

# ----------------------------
# Exercicio 4
# a.

def somaDivisores(num):
    """

    Parameters
    ----------
    num : INT
        Número de input.

    Returns
    -------
    soma : INT
        Soma dos divisores.

    """
    soma = 0
    
    for i in range(1, num):
        if num % i == 0:
            soma += i
            
    return soma
    
print("4. a. Ans.: Deve receber um número inteiro maior que 0")

# b.
print("4. b. Ans.: Deve retornar um número inteiro correspondente à soma dos divisores do número dado maiores que 1 e menores que o número dado")

# ----------------------------
# Exercicio 5
"""5. Crie um programa que pergunte sucessivamente ao utilizador um número inteiro positivo e
imprima a soma dos seus divisores. A execução do programa deve terminar quando o
utilizador introduzir um número negativo. Nota: assuma que a função somaDivisores,
apresentada no exercício anterior, se encontra definida."""


posInteger = 1

while posInteger > 0:
    posInteger
    print(somaDivisores(posInteger))
    posInteger = int(input("Give me a positive integer: "))



# ----------------------------
# Exercicio 6

DIA_ATUAL = 4
MES_ATUAL = 11
ANO_ATUAL = 2020

anoMae = 1964
mesMae = 11
diaMae = 4

anoPai = 1949
mesPai = 3
diaPai = 31

def quantosAnos(parent, dia_parent, mes_parent, ano_parent):
    if mes_parent > MES_ATUAL or \
        (mes_parent == MES_ATUAL and dia_parent > DIA_ATUAL):
            print(f'{parent} tem', ANO_ATUAL - ano_parent - 1, 'ano(s)')
    else:
        print(f'{parent} tem', ANO_ATUAL - ano_parent, 'ano(s)')
      
quantosAnos("Mãe", diaMae, mesMae, anoMae)
quantosAnos("Pai", diaPai, mesPai, anoPai)

"""
7. Escreva uma função que receba dois números inteiros e devolva o maior deles. Inclua o
contrato da função. Teste a função escrevendo um programa que receba dois números
inteiros do utilizador e imprima o resultado da chamada à função desenvolvida. Como teria
de fazer para determinar o menor de dois números com uma segunda função que tirasse
partido de chamar a primeira?
"""

def max_num(x, y):
    """
    

    Parameters
    ----------
    x : INT
        First number.
    y : INT
        Second.

    Returns
    -------
    INT
        The biggest number of the two inputs.

    """
    if x > y:
        return x
    else:
        return y
        
def minimum(a,b):
    return a if b == max_num(a,b) else a

print(max_num(7,8))
print(minimum(5,6))



"""
8. Escreva uma função (o contrato não pode ser esquecido) que elimine a casa das unidades de
um número inteiro. Por exemplo, retira(537) devolve 53. Se o argumento só tiver algarismo
das unidades, a função deve devolver 0. Teste a função escrevendo um programa que
receba um número inteiro do utilizador e imprima o resultado de chamada à função
desenvolvida.
"""

def rounder(x):
    """
    Divides by 10 with floor

    Parameters
    ----------
    x : INT
        Number to be shortened.

    Returns
    -------
    INT
        Returns the input number without the last digit.

    """
    return x // 10

def formatter(x):
    """
    

    Parameters
    ----------
    x : INT
        Number to be shortened.

    Returns
    -------
    INT
        Returns the input number without the last digit.

    """
    a = str(x)
    if len(a) > 1:
        return a[:-1]
    else:
        return 0
    


print(rounder(2))
print(formatter(77))

"""
9. Escreva uma função que acrescente um 0 no final de um número inteiro. Por exemplo,
aumenta(73) devolve 730 (se esquecer o contrato a definição da função está incompleta).
aumenta(0) deve devolver 0. Teste a função escrevendo um programa que receba um
número inteiro do utilizador e imprima o resultado da chamada à função desenvolvida.
"""

def by10(x):
    """
    

    Parameters
    ----------
    x : INT
        DESCRIPTION.

    Returns
    -------
    INT
        DESCRIPTION.

    """
    return x*10

print(by10(55))

def add_zero(x):
    """
    

    Parameters
    ----------
    x : INT
        DESCRIPTION.

    Returns
    -------
    x : INT
        DESCRIPTION.

    """
    x = int(str(x)+'0')
    return x

print(add_zero(55))


"""
10. Escreva uma função que receba um número inteiro e devolva a soma dos divisores próprios
desse número

Teste a função escrevendo um programa que receba um número inteiro do utilizador e
imprima o resultado da chamada à função desenvolvida.
"""


def somaDivisores10(num):
    """
    Soma dos divisores próprios de um numero dado.
    Requires:
    num seja int e num > 0
    Ensures:
    um int correspondente à soma dos divisores
    de num que sejam maiores que 1 e menores que num

    Parameters
    ----------
    num : INT
        Número de input.

    Returns
    -------
    soma : INT
        Soma dos divisores, incluindo o próprio elemento.

    """
    soma = 0
    
    for i in range(1, num+1):
        if num % i == 0:
            soma += i
            
    return soma

user_num = int(input("Give me a number: "))

print(somaDivisores10(user_num))

"""
11. Escreva uma função que verifique se um dado número dado é primo. Relembre que um
número é primo se é maior do que 1 e não tem divisores próprios. Teste a função
escrevendo um programa que receba um número inteiro do utilizador e imprima o resultado
da chamada à função desenvolvida."""

def chekcNrPrimo(primo):
    if somaDivisores10(primo) == primo+1:
        is_nr_primo = True
    else:
        is_nr_primo = False
    return is_nr_primo


n_primo_teste = int(input("Give me a number: "))
print(chekcNrPrimo(n_primo_teste))

"""
12. Usando a função do exercício anterior, escreva um programa que receba um número inteiro
n maior do que 2 e escreva no ecrã quantos números primos existem entre 2 e n (inclusive).
Por exemplo, existem 1000 números primos entre 2 e 7919. Teste a função escrevendo um
programa que receba um número inteiro do utilizador e imprima o resultado de chamada à
função desenvolvida. Explique, nesta situação, em que difere a programação defensiva da
programação por contratos.
"""

def quantosPrimos(num):

    count = 0
    print(num)
    for i in range(3, (num+1)):
        print("i", i)
        print("count", count)
        is_primo = chekcNrPrimo(i)
        count += is_primo
    # Count + 1 because the chekcNrPrimo function doesn't count the number itself
    return count+1

bigger_than_2 = False
while bigger_than_2 == False:
    num_bigger_than_2 = int(input("Give me a number: "))
    if num_bigger_than_2 > 2:
        bigger_than_2 = True
        print("num bigger than 2", num_bigger_than_2)
        print(quantosPrimos(num_bigger_than_2))





