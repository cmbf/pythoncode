# -*- coding: utf-8 -*-
# ----------------------------
# @author: Cristiano Ferro (utilizador)
# Create on Thu Oct 29 09:46:38 2020
# ----------------------------
# Ficha 3

from functools import reduce

# 1.
# a.
a1 = map(lambda x:x+1, range(1,4))
print('1. a. ', list(a1))

# b.
b1 = map(lambda x:x>0, [3,-5,-2,0])
print('1. b. ', list(b1))

# c.
c1 = filter(lambda x:x>5, range(1,7))
print('1. c. ', list(c1))

# d.
d1 = filter(lambda x:x%2==0, range(1,11))
print('1. d. ', list(d1))

# 2.
# a.
a2 = reduce(lambda y, z: y* 3+z, range(1,5))
print('2. a. ', a2)

# b.
b2 = reduce (lambda x, y: x** 2+y, range(2,6))
print('2. b. ', b2)

