# -*- coding: utf-8 -*-
# ----------------------------
# @author: Cristiano Ferro (utilizador)
# Create on Mon Oct 26 08:55:15 2020
# ----------------------------

# Slide 49
# ----------------------------

k = ['red', 'green']
v = ['#FF0000', '#008000']
z = zip(k,v)
d = dict(z)
print(d)


# How many students have an s in the name

# Slide 61
# ----------------------------
family = {
    'child':{'name':'James' , 'age': 8},
    'child2':{'name':'Emma' , 'age': 20}
    }
print(family)

d1 = {'name':'James' , 'age': 8},
d2 = {'name':'Emma' , 'age': 20}

family = {
    'child1':d1,
    'child2':d2
    }

# Slide 62
# ----------------------------

d = {
     'F': 0,
     'B': 0
     }

# Randomly choose between dictionary keys
import random

for _ in range(17):
    d[random.choice(list(d.keys()))] += 1
    
print(d)
try:
    x = {'key1': 'value1 from x', 'key2': 'value2 from x'}
    y = {'key1': 'value1 from x', 'key2': 'value2 from x'}
    print(x | y)
    print(y | x)
except:
    print('please check if you are using python version 3.9 or higher')

# ----------------------------
    # Set presentation
# ----------------------------

# Slide 16
# ----------------------------
f = ('apple', 'orange', 'banana')
set(f)
print(f)
print(len(f))
print(type(f))

for i in f:
    print(i)
    
n = set(['a','b'])
print(n)

m = set (('orange', 'banana', 'apple'))

print(f == m)
print('cherry' in f)

f = {'apple', 'orange', 'banana'}

f.add('cherry')
print(f)

f.update(['mango', 'grapes'])
print(f)

f.remove('apple')
print(f)

# Slide 17
f = {'apple', 'orange', 'banana'}

f = {'a', 'b', 'c'}
f.add('d')
f.add(('e2','f2')) # with tuple doesn't get an error

f.update('g')
f.update('h', 'i')

f = {'a', 'b', 'c'}
f.update(['h', 'i'])

print(f)


# Slide 19
# ----------------------------
vowels = {'a', 'e', 'o', 'i', 'u'}

# 'k' if not in vowels
# vowels.remove('k') # Error keyError

vowels.discard('k')

v2 = vowels

c = vowels.copy()

print(vowels)

x = vowels.pop() #randomly removes one member of the set

print(x)
print(vowels)
print(v2)
print(c)

vowels.clear()
print(vowels)
print(len(vowels))

del c

# Slide 20
# ----------------------------
X = {1, 2, 3}
Y = {2, 3, 4}
print(X.union(Y))
print(X | Y)
# union doesn't change/update the sets

X = {1, 2, 3}
Y = {2, 3, 4}
print(X.intersection(Y))
print(X & Y)
# intersection doesn't change/update the sets

X = {1, 2, 3}
Y = {2, 3, 4}
X.update(Y)
print(X)


# Slide 21
# ----------------------------
A = {1, 2, 3, 4, 5}
B = {2, 4, 7}

print(A-B)
print(B-A)

r = A.difference(B)
print(r)
print(A)
print(B)

X = {1, 2, 3}
Y = {2, 3, 4}

print(X.symmetric_difference(Y))
print(X ^ Y)                            # ^ is XOR
print(X.union(Y) - X.intersection(Y))   # same result {1, 4}
print(X.union(Y) - Y.intersection(X))   # same result {1, 4}

r = A.difference_update(B)
print(r)                                # None
print(A)
print(B)


# Slide 26
# ----------------------------
d = {'one': 1, 'two': 2}
print('one' in d)
print(1 in d)                           # There's no key 1

# ----------------------------
# Does it intersect

X = {1, 2}
Y = {1, 2, 3}
print(X.isdisjoint(Y))

X = {1, 2}
Y = {3, 7, 8}
print(X.isdisjoint(Y))

# Slide 27
# ----------------------------
# Subset

A = {1, 2, 4}
B = {1, 2, 3, 4, 5}

print(A.issubset(B))
print(B.issubset(A))

C = set()

print(C.issubset(A))
print(A.issubset(C))

dict([('a', 1), ('b', 2)])

dict(a = 1, b = 2, c = 3)

for _title in [3, 7, 11, 15, 19]:
    if _title not in [7, 15, 10]:
        _title += 2
        print("second loop: ", _title)
        