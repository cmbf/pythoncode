# -*- coding: utf-8 -*-
"""
@author: Cristiano Ferro %(username)
Created on %(date) 
"""
# ----------------------------
# Class Exercises

# Exercise 1 - Slide 23
# ----------------------------

A = {1, 2, 3, 4, 6, 9, 10}
B = {1, 3, 4, 9, 13, 14, 15}
C = {1, 2, 3, 6, 9, 11, 12, 14, 15}

# C - (intersection of A and B)
answer_1_1 = C - A.intersection(B)
print(answer_1_1)
answer_1_2 = C - A.intersection(B , C)
print(answer_1_2)
answer_1_3 = C.difference(A & B & C)
print(answer_1_3)

# (intersection of A and B) - C
answer_2_1 = (A & B) - C
print(answer_2_1)
answer_2_2 = A & B - C
print(answer_2_2)
answer_2_3 = A.intersection(B) - C
print(answer_2_3)

# (Union of A and B) - C
answer_3_1 = A.union(B) - C
print(answer_3_1)
answer_3_2 = A.union(B) - C              # Can't be A + B - C
print(answer_3_2)
answer_3_3 = (A | B) - C                 # Needs parenthese for correct result
print(answer_3_3)


# Exercise 2 - Slide 29
# ----------------------------
# Which characters of 'a', 'y', 'c', 'o', 'z' are in 'Python Course' ? - use set

char = {'a', 'y', 'c', 'o', 'z'}
charIntersect = {}
pyCourse = 'Python Course'
pyCourse = set(pyCourse)

for i in pyCourse:
    if not char.isdisjoint(i):
        charIntersect(i)

# Slide solution
w_set = set(pyCourse)
print(char.intersection(w_set))
print(w_set.intersection(char))

