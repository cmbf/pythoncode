# -*- coding: utf-8 -*-
"""
@author: Cristiano Ferro %(username)
Created on %(date) 
"""
# ----------------------------

# Homework 1 - Slide 33
# ----------------------------
d1 = {'a':1, 'b':3, 'c':2}
d2 = {'a':2, 'b':3, 'c':1}

nSet1 = set(d1.items())
nSet2 = set(d2.items())

interSect = nSet1.intersection(nSet2)
print(interSect)

nS = dict(interSect)
print(nS)
