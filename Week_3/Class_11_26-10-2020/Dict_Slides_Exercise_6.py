# -*- coding: utf-8 -*-
"""
@author: Cristiano Ferro %(username)
Created on %(date) 
"""
# ----------------------------
# Exercise 6 - Slide 51
# ----------------------------
# Find nr of occurrences of X in S

s = 'Python Course'
x = ['o', 'r']

def findOcc(inStr, inList):
    occ = 0
    for i in inStr:
        if i in inList:
            occ += 1
    print(inList,':', occ)

for i in x:
    findOcc(s, i)

# Professor's solution - same code as in class 10
d = {}
for i in s:
    if i in x:
        d.setdefault(i,0)
        d[i] += 1
print(d)

