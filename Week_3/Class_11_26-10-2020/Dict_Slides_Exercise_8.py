# -*- coding: utf-8 -*-
"""
@author: Cristiano Ferro %(username)
Created on %(date) 
"""
# ----------------------------
# Exercise 8

students = [
            {'id':123, 'name': 'Sophia', 's': True},
            {'id':378, 'name': 'William', 's': False},
            {'id':924, 'name': 'Sara', 's': True}
            ]

print(students)

# Booleans can be counted! True has the value 1, False has the value 0
# ----------------------------
# Solution 1
counTrue = 0
for i in students:
    if i['s'] == True:
        counTrue += 1
print(counTrue)

# ----------------------------
# Solution 2 - (1 of slides)
count_true = 0    
for i in students:
    # if i['s'] == True:
    #     d += 1
    # better to convert to int just in case another version of python doesn't work
    count_true += int(i['s'])
print(count_true)

# ----------------------------
# Solution 3 - (2 of slides)
print(sum(d['s'] for d in students))
print(students[1])