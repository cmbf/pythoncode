# -*- coding: utf-8 -*-
# ----------------------------
# @author: Cristiano Ferro (utilizador)
# Create on Mon Oct 26 14:29:51 2020
# ----------------------------
import random as rand

# Homework 2
# ----------------------------

def includedOrNot(included):
    if included == "Y" or "y":
        included = 1
    elif included == "N" or "n":
        included = 0
    else:
        print("Answer not understood, will be chosen at random instead")
        included = rand.randint(0,1)
    return included

nr1 = int(input("Interval 1 Number 1: "))
included1 = includedOrNot(input("Is this one included? Y/N? "))
nr2 = int(input("Interval 1 Number 2: "))
included2 = includedOrNot(input("Is this one included? Y/N? "))
nr3 = int(input("Interval 2 Number 1: "))
included3 = includedOrNot(input("Is this one included? Y/N? "))
nr4 = int(input("Interval 2 Number 2: "))
included4 = includedOrNot(input("Is this one included? Y/N? "))

print(nr1, included1, 'to', nr2, included2, ' ;', nr3, included3, 'to', nr4, included4 )

# Checks which number is bigger and creates range or checks if it's a range at all
if nr1 > nr2:
    if not included1:
        nr1 -= 1
    elif not included2:
        nr2 += 1
    range1 = set(range(nr2, nr1+1))
elif nr1 < nr2:
    if not included1:
        nr1 += 1
    elif not included2:
        nr2 -= 1
    range1 = set(range(nr1, nr2+1))
elif nr1 == nr2:
    raise Exception("This is not a interval")
else:
    print("Interval 1 not a valid interval")
print("Interval 1:", range1)

# Checks the same again for range 2
if nr3 > nr4:
    if not included3:
        nr3 -= 1
    elif not included4:
        nr4 += 1
    range2 = set(range(nr4, nr3+1))
elif nr3 < nr4:
    if not included3:
        nr3 += 1
    elif not included4:
        nr4 -= 1
    range2 = set(range(nr3, nr4+1))
elif nr3 == nr4:
    raise Exception("This is not a interval")
else:
    print("Interval 2 not a valid interval")
# range2_list = list(range2).sort()
print("Interval 2:", range2)

# Will check if there is an intersection and print if there is
if not range1.isdisjoint(range2):
    range3 = range1.intersection(range2)
    print("Intersection: ", end='')
    final_range = list(range3)
    final_range.sort()
    print(final_range)
else:
    print('There is no intersection')