# -*- coding: utf-8 -*-
"""
@author: Cristiano Ferro %(username)
Created on %(date) 
"""
# ----------------------------
# Exercise 7

# Z and y keys can't be duplicated
d = {'x':3, 'y':2, 'z':1}

r = {}
for k,v in d.items():
    if k not in r.keys():
        r[k] = v

print(r)
