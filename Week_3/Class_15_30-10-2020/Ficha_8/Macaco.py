5# -*- coding: utf-8 -*-
# ----------------------------
# @author: Cristiano Ferro (utilizador)
# Create on Fri Oct 30 09:16:07 2020
# ----------------------------
class Macaco:
    def __init__(self, nome):
        self.nome = nome
        self.bucho = []

    def comer(self, comida):
        print('comeu:', comida, end='. ')
        self.bucho.append(comida)
        print('No seu bucho está a seguinte comida:')
        self.verBucho()
        # adicionar comida à lista de comida no bucho
        
    def verBucho(self):
        for i in self.bucho:            
            print(i)
        # Ver a lista de comida no bucho
        
    def digerir(self, alimento_digerido):
        self.bucho.remove(alimento_digerido)
        # eliminar da lista de comida no bucho
        
macaquinho = Macaco('macaquito')

macaquinho.comer('Banana')
macaquinho.comer('Chocolate')
macaquito2 = Macaco('macaquito2')
macaquinho.comer(macaquito2) # O macaco conseguiu comer o outro... RIP macaquito2
macaquinho.comer(macaquito2.nome)
macaquinho.comer('Chocolate')

