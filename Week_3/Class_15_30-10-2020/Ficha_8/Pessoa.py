# -*- coding: utf-8 -*-
# ----------------------------
# @author: Cristiano Ferro (utilizador)
# Create on Fri Oct 30 08:41:37 2020
# ----------------------------

class Pessoa:
    def __init__(self, idade, peso, altura, nome):
        self.idade = idade
        self.peso = peso
        self.altura = altura
        self.nome = nome
    
    def envelhecer(self, anos):
        nova_idade = self.idade + anos
        while self.idade < 21 and self.idade < nova_idade:
            self.crescer(0.005)
            self.idade += 1
        return nova_idade
        
    def engordar(self, peso_ganho):
        self.peso += peso_ganho
        
    def emagrecer(self, peso_perdido):
        self.peso -= peso_perdido
        
    def crescer(self, m):
        self.altura += m
        # porquê que o round fica sempre com 2 casas?
        self.altura = round(self.altura,3)
    

cris = Pessoa(10, 55, 1.75, 'Cristiano')
cris.envelhecer(6)
print(cris.idade)
print(cris.altura)
