# -*- coding: utf-8 -*-
# ----------------------------
# @author: Cristiano Ferro (utilizador)
# Create on Fri Oct 30 11:30:26 2020
# ----------------------------

# 1.
# b.
# ----------------------------
import math

def perimetro_quadrado(lado):
    return lado*4
    
def area_quadrado(lado):
    return lado*lado

def ladoPelaDiagonal(diagonal):
    return (diagonal*math.sqrt(2)) / 2