# -*- coding: utf-8 -*-
# ----------------------------
# @author: Cristiano Ferro (utilizador)
# Create on Fri Oct 30 11:31:00 2020
# ----------------------------

# 1.
# c.
# ----------------------------

import a_operacoes_circulos
import b_operacoes_quadrados

valor = int(input('valor: '))

print(a_operacoes_circulos.perimetro_circulo(valor))
print(a_operacoes_circulos.area_circulo(valor))

print(b_operacoes_quadrados.perimetro_quadrado(valor))
print(b_operacoes_quadrados.area_quadrado(valor))