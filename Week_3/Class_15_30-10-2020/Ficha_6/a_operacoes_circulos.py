# -*- coding: utf-8 -*-
# ----------------------------
# @author: Cristiano Ferro (utilizador)
# Create on Fri Oct 30 11:26:12 2020
# ----------------------------

# 1.
# a.
# ----------------------------
import math

def perimetro_circulo(r):
    return 2*math.pi*r
    
def area_circulo(r):
    return math.pi*(r**2)