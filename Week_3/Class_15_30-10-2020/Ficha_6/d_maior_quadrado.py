# -*- coding: utf-8 -*-
# ----------------------------
# @author: Cristiano Ferro (utilizador)
# Create on Fri Oct 30 11:32:30 2020
# ----------------------------

# 1.
# d.
# ----------------------------

# 2r == diagonal

#perimetro e area do quadrado

# diagonal = sqrt(2*r*r)

import a_operacoes_circulos
import b_operacoes_quadrados
import math

raio = int(input('valor: '))

diagonal = raio*2
lado = b_operacoes_quadrados.ladoPelaDiagonal(diagonal)

print(lado)
print(b_operacoes_quadrados.perimetro_quadrado(lado))
print(b_operacoes_quadrados.area_quadrado(lado))
