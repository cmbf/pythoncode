# -*- coding: utf-8 -*-
# ----------------------------
# @author: Cristiano Ferro (utilizador)
# Create on Fri Oct 30 10:42:23 2020
# ----------------------------
class House:
    
    area = 120 # variaveis da classe
    window_size = (2,3)
    
    def __init(self, cor_das_paredes=0x000000, cor_do_telhado=0x0000ff):
        self.cor_das_paredes = cor_das_paredes
        self.cor_do_telhado = cor_do_telhado
    
    #funções da classe
    def abre_porta(self):
        pass

print(House.area)
print(House.abre_porta())

House()

concrete_house = House(0xffffff, 0x000fff)
concrete_house.area = 450


print(House.area)
print(concrete_house.cor_das_paredes)
print(concrete_house.cor_do_telhado)

another_house = House()
another_house.cor_do_telhado
another_house.cor_das_paredes
another_house.portas = 2
print("quantas portas? " + str(another_house.portas()))
del(another_house.portas)
del(another_house.area)
print("qual a area da casa= " + str(another_house.area))
and_another_house = House()
print(and_another_house.area())

class Pais:
    def __init(self, nome, idade):
        self.nome = nome
        self.idade = idade
    
    def trabalhar(self):
        print("trabalhar")

class Eu(Pais):
    def __init__(self, nome, idade, hobby):
        super().__init__(nome, idade)
        # é usado o super porque eu quero definir a idade de eu como idade e o nome de eu
        # self.idade = idade + 1
        # 
        self.hobby = hobby
    def trabalhar(self):
        super().trabalhar()
        print("e trablhar mais")
        
    def compassion(self):
        print("c")

eu = Eu('Cris', 23, "Jogar à bola")
print("O meu nome é: " + str(eu.nome))
print("A minha idade é: " + str(eu.idade))
print("O meu hobby é: " + str(eu.hobby))
eu.trabalhar()
print(f"I have compassion. {eu.compassion}")

class EstruturaDeDados:
    pass

estrutura = EstruturaDeDados()
estrutura.x = 2
