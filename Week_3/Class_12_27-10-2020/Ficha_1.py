# -*- coding: utf-8 -*-
"""
@author: Cristiano Ferro %(username)
Created on %(date) 
"""
# ----------------------------

import datetime

# Exercise 1
# ----------------------------
# Escreva em linguagem python um programa que leia uma temperatura em graus 
# Fahrenheit e que converta em celcius.
# Write in python language a program that reads a temperature in Fahrenheit 
# degrees and converts to celcius.

# C = (F - 32)/18
# a = float(input('What is the temperature in ºF: '))
70
def farToCel(F):
    C = (F - 32)//1.8
    return C

# Exercise 2
# ----------------------------
# Escreva um programa que peça o ano de nascimento e que retorne a idade que 
# a pessoa vai ter ao fim de um ano

birthYear = int(input("In what year were you born: "))

def ageEndYr(birthYear):
    currYear = datetime.datetime.now().year
    age = currYear - birthYear
    return age
print("At the end of the year you'll have", ageEndYr(birthYear), "years")

# Exercise 3
# ----------------------------
# Escreva um programa que leia uma hora em horas, minutos, segundos e que 
# traduza para segundos
# h = '01 30 22'
time = datetime.datetime.now().strftime("%X")

def hourToSec(time):
    h_components = time.split(":")
    segundos_total = int(h_components[0])*3600 + int(h_components[1])*60 + int(h_components[2])
    return segundos_total

print("There are", hourToSec(time), "seconds in", time)

# Exercise 4
# ----------------------------
# Desenvolva a versão inversa do problema anterior: lê um número representando uma
# duração em segundos e imprime o seu equivalente em horas, minutos e segundos. Por
# exemplo, 9999 segundos é equivalente a 2 horas, 46 minutos e 39 segundos
import random

secs = random.randint(1,99999)
# secs = 3600
def secToHour(seconds):
    mins = seconds // 60
    hours = mins // 60
    return hours
print(secToHour(secs))
if secToHour(secs) <= 1:
    print(secs, "seconds is", secToHour(secs), "hour.")
else:
    print(secs, "seconds are ", secToHour(secs), "hours.")
    
    
# Exercise 5
# ----------------------------
# Qual valor inicial de x para que no final a execução seja -1
# a.
print('5. a. \nO valor de x é 1 no fim se for 1 no início')


# b. Qual teria de ser o valor inicial de x para que no final da execução fosse -1?
print('5. b. \nTeria que ser 0 no início para ser -1 no fim')

# c. Uma parte deste programa que nunca é executada
print('5. c. \nA linha que nunca é executada é a 3')

# Exercise 6 - ficheiro romanos.py

# Exercise 7
# ----------------------------
# Write in python language a program that reads a year (>0) and writes the 
# corresponding century
# import random

# Solution 1
# year = random.randint(1, 2020)
year = 1900
yrStr = str(year)
print(yrStr[-2:])
if year < 0:
    year = 0
else:
    if yrStr[-2:] == '00':
        century = (year // 100)
    else:
        # 2000 - 19 
        century = (year // 100) + 1
print('year', year, 'century', century)

# Solution 2 - by teacher
ano = 1898
temp = ano // 100
rest = ano % 100
print(temp if rest==0 else temp+1)

# Exercise 8 - ficheiro add_dias.py

# Exercise 9
# ----------------------------
# Indique os erros sintáticos
print('9. The correct operator in \'x = y\' should be \'x == y\'')
print('Needs a colon after the condition, \'while x = 1 and y < 5:\'')

# Exercise 10
# ----------------------------
# Este programa em Python tem como objectivo escrever a tabuada do número inteiro dado
# pelo utilizador. Explique porque é que este programa não termina, corrija o erro e
# especifique qual o valor das variáveis n e i no final da execução do programa corrigido:
print('10.')
print('O programa não termina porque o valor de i não é incrementado.')
print('"i + 1" precisa de estar "assigned" a i, portanto "i += 1"')

n = int(input("Escreve um número inteiro: "))
print("Tabuada do", n, ":")
i = 1
while i <= 10:
    print(n, "x", i, "=", n * i)
    i += 1

print('O valor da variável "n" não foi alterado durante a execução, apenas usado para modificar executar operações')
print('O valor da variável i no fim é sempre 10 uma vez que no início é 1 e é incrementado até a condição do while loop fazer terminar o programa, que é quando o i chega a 10')

# Exercise 11
# ----------------------------
# Considere este programa em Python:
print('11.')
print('a. Este programa divide faz uma divisão sem utilizar o operador de divisão')
print('b. Divisão (/) e Modulo (%). No entanto retornam diferentes partes da operação - um retorna o quociente e outro retorna o resto')
print('c. e d.')

dividendo = int(input("Dividendo: "))
divisor = int(input("Divisor: "))
while dividendo < divisor or dividendo < 0 or divisor < 0:
    print("Por favor certifique-se que o dividendo não é maior que o divisor ou que um deles não é número negativo")
    dividendo = int(input("Dividendo: "))
    divisor = int(input("Divisor: "))
resto = dividendo
quociente = 0
while resto >= divisor:
    resto = resto - divisor
    quociente = quociente + 1
    print("O quociente é", quociente, "e o resto é", resto)

# Exercise 12
# ----------------------------
# O seguinte programa em Python escreve no ecrã os factoriais de todos os números inteiros
# entre 1 e n, em que n é dado pelo utilizador:
# Altere o programa de forma a que o programa não escreva os factoriais cujo valor é superior
# a 1000. (Note que há formas mais ou menos eficientes de o fazer.)


n = int(input("Escreve um número inteiro: "))
current_n = 1
while current_n <= n:
    i = current_n
    f = 1
    while i > 1:
        f = f * i
        i = i - 1
    if f > 1000:
        print("Valor do factorial é maior que 1000")
        break
    print("Factorial de " + str(current_n) + ": " + str(f))
    current_n = current_n + 1


# Exercise 13
# ----------------------------
# Escreva um programa em Python que peça ao utilizador um número decimal e escreva no
# ecrã a sua parte inteira perguntando em seguida se o utilizador quer terminar a utilização do
# programa.
def extractInt(decimNr):
    if "." in decimNr:
        integerNr = decimNr.split(".")[0]
    elif "," in decimNr:
        integerNr = decimNr.split(",")[0]
    else:
        return
    return int(integerNr)

endProgram =  False
while not endProgram:
    print(extractInt(input("Give me a decimal number: ")))
    askEnd = input("End program? (y/n) ")
    if askEnd == "y" or askEnd == "Y":
        endProgram = True
    elif askEnd == "n" or askEnd == "N":
        continue
    else:
        print("Answer not understood by the program")

# Exercise 14
# ----------------------------
# Escreva um programa em Python que peça ao utilizador um número inteiro k e uma palavra
# w e escreva no ecrã k linhas em que cada linha i tem o número da linha i separado por um
# espaço da palavra w concatenada i vezes, com i no intervalo de 1 a k.


def intPalavra(k, w):
    for i in range(k):
        print(i, w*i)

intPalavra(int(input("Número \"k\": ")),input("Palavra \"w\": "))

# Exercise 15
# ----------------------------
# Escreva um programa em Python que peça ao utilizador um número inteiro k e escreva no
# ecrã o resultado do piatório das potências de 3 com expoente de 0 a k.



# Exercise 16
# ----------------------------
# Escreva um programa em Python que que peça ao utilizador um número inteiro k maior do
# que 2 e escreva no ecrã quantos números perfeitos existem entre 2 e k (inclusive). Por
# exemplo, existem 4 números perfeitos entre 2 e 10000 (o 6, o 28, o 496 e o 8128).




# Exercise 17
# ----------------------------
# Escreva um programa em Python que peça ao utilizador um número inteiro k maior do que
# 10 e escreva no ecrã quantas capícuas existem entre 10 e k. Uma capícua é um número que
# se lê de igual forma da esquerda para a direita e da direita para a esquerda. Por exemplo,
# entre 10 e 100 existem 9 capícuas

k = int(input("Write a number bigger than 10: "))

def capicua(k):
    capicuas = []
    if k > 10:
        for i in range(10, k+1):
            if str(i) == str(i)[::-1]:
                capicuas.append(i)
            print(i)
        print("There are ", len(capicuas), "Capicuas")
    else:
        print("Number might be smaller than 10")
capicua(k)


 










