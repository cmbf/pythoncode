# -*- coding: utf-8 -*-
# ----------------------------
# @author: Cristiano Ferro (utilizador)
# Create on Tue Oct 27 08:47:41 2020
# ----------------------------
import datetime

# Exercise F to C
# ----------------------------
# Escreva em linguagem python um programa que leia uma temperatura em graus 
# Fahrenheit e que converta em celcius.
# Write in python language a program that reads a temperature in Fahrenheit 
# degrees and converts to celcius.

# C = (F - 32)/18
# a = float(input('What is the temperature in ºF: '))
70
def farToCel(F):
    C = (F - 32)//1.8
    return C

# ----------------------------
# Escreva um programa que peça o ano de nascimento e que retorne a idade que 
# a pessoa vai ter ao fim de um ano

# birthYear = int(input("In what year were you born: "))

def ageEndYr(birthYear):
    currYear = datetime.datetime.now().year
    age = currYear - birthYear
    return age
# print(ageEndYr(birthYear))

# ----------------------------
# Escreva um programa que leia uma hora em horas, minutos, segundos e que 
# traduza para segundos
h = '01 30 22'
h_components = h.split()
print(h_components)
segundos_total = int(h_components[0])*3600 + int(h_components[1])*60 + int(h_components[2])
print(segundos_total)
# seconds = datetime.datetime.now().seconds
# def hourToSec():
#     hr = 50
#     mins = hr * 60
#     s = mins * 60
#     print(hr, 'hours')
#     print(mins, 'minutes')
#     print(s, 'seconds')

# ----------------------------
# Qual valor inicial de x para que no final a execução seja -1
# b. Uma parte deste programa que nunca é executada
x = 1
if x == 1:
    x = x + 1
    if x == 1:          # b.
        x = x + 1
    else:
        x = x - 1
else:
    x = x - 1
        
print(x)


# ----------------------------
# Escreva um programa em python que recebe um número inteiro que o represente
# em notação romana

# num_romana = {'M' : 1000, 'D' : 500, 'C' : 100, 'L' : 50, 'X' : 10, 'V' : 5, 'I' : 1, 'IV' : 4}
casos especiais = {'I' : 1 , 'IV' : 4 , 'V' : 5, 'X' : 10 , 'L' : 50 ,  'C' : 100 , 'D' : 500 , 'M' : 1000}
ordem_dos_especiais_inversa = {1000 : 'M' , 500 : 'D', 100 : 'C' , 50 : 'L' , 10 : 'X' , 5 : 'V' , 4 : 'IV' , 1 : 'I'}

number_op = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 4, 1]

a = 33
roman_number = ''

for x in number_op:
    quociente = a // x
    if quociente != 0:
        for _ in range(quociente):
            roman_number += num_romana[x]
    
    a = a%x



# ----------------------------
# Write in python language a program that reads a year (>0) and writes the 
# corresponding century
# import random

# Solution 1 - with restrictions
# year = random.randint(1, 2020)
year = 1000
yrStr = str(year)
if year < 0:
    year = 0
else:
    if yrStr[-3:-1] == '00':
        century = (year // 100)
    else:
        # 2000 - 19 
        century = (year // 100) + 1

print('year', year, 'century', century)

# Solution 2 - by teacher
ano = 1898
temp = ano // 100
rest = ano % 100
print(temp if rest==0 else temp+1)



