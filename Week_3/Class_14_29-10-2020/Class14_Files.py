# -*- coding: utf-8 -*-
# ----------------------------
# @author: Cristiano Ferro (utilizador)
# Create on Thu Oct 29 10:47:06 2020
# ----------------------------

# w = open('loremIpsum.txt','w')
# Escreve info para um ficheiro, apagando o que estava escrito
# a = open('loremIpsum.txt','a')
# Escreve informação para um ficheiro, sem apagar o que lá estava
# x = open('loremIpsum.txt','x')
# Cria um ficheiro e dá erro se o ficheiro existir
r = open('loremIpsum.txt','r')
# Abre um ficheiro para leitura

# print(r.read(5))
print(r.readline())
print(r.readline())
print(r.readline())
print(r.closed) # Check if file is closed
r.close() 
# File 2
with open('loremIpsum1.txt') as file_reader:
    for line in file_reader:
        print(line)
print(r.closed) # Check if file is closed
print(file_reader.closed) # Check if file is closed


# os module python
import os
print(os.path.abspath('loremIpsum.txt'))
print(os.path.relpath('loremIpsum.txt'))
print(os.path.basename('./loremIpsum.txt'))

import copy
# os.remove('loremIpsum.txt')
lines = []
file_writer = []
lines_for_writing = []
lines_after_writing = []
with open('loremIpsum1.txt') as file_reader:
    lines = file_reader.readlines()
    lines_for_writing = copy.deepcopy(lines)
    for line in lines_for_writing:
        line.replace('s','p')
        print(lines)
    
with open('loremIpsum1.txt') as file_reader:
    lines_after_writing = file_reader.readlines()
    print(lines_after_writing == lines)
    
try:
    int('abc')
except ValueError:
    print('the string must contain number, not letters')
except: # se for um ficheiro, com isto fecha sempre?
    print("Something went wrong. But life's like that, so focus on doing better each time")
finally:
    print('fancy')

f = open('loremIpsum.txt')
try:
    # So something with file content
except expression as identifier:
    # Handle exception
finally:
    f.close()

with open ('loremIpsum1.txt') as file_reader:
    try:
        file_reader.readlines()
        
with open ('loremIpsum1.txt') as csvfile:
    spamreader = csv.(csvfile)
