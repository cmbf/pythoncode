# -*- coding: utf-8 -*-
# ----------------------------
# @author: Cristiano Ferro (utilizador)
# Create on Fri Oct 30 08:59:13 2020
# ----------------------------
# FICHA 4

# ----------------------------
# Exercicio 4

from statistics import mean

# Solução 1
def medias1(ficheiro):
    with open(ficheiro) as file_reader:
        for line in file_reader:
            line = line.strip()
            split_line = line.split()
            # Segunda solução:
            # ----------------------------
            # for i in range():
            #     split_line[i] = float(split_line[i])
            print(split_line)
            float_list = list(map(float, split_line))
            print(round(mean(float_list),2))

# Solução 2, a que continua a ser usada mais à frente
def medias(ficheiro):
    try:
        file_reader = open(ficheiro)
        try:
            for line in file_reader:
                line = line.strip()
                split_line = line.split()
                # Segunda solução:
                # ----------------------------
                # for i in range():
                #     split_line[i] = float(split_line[i])
                print(split_line)
                float_list = list(map(float, split_line))
                print(round(mean(float_list),2))
        except:
            print("Something went wrong while reading the file")
        finally:
            file_reader.close()
    except:
        print("Something went wrong while trying to access file")