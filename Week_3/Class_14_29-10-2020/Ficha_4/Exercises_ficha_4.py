# -*- coding: utf-8 -*-
"""
@author: Cristiano Ferro %(username)
Created on %(date) 
"""
# ----------------------------
# FICHA 4


# ----------------------------
# Exercise 1.
# Qual o resultado dos 3 programas
# 1)
# writes the file / creates new
print('\nExercise 1.1')
print("\nAns.: Writes the file, deleting content (overriding) / creates new if it doesn't exist and prints numbers from 0 to 99 in a single line")
out_file = open("outFile.txt", 'w')
for i in range(100):
    out_file.write(str(i))
out_file.close()

# 2)
print('\nExercise 1.2')
print("\nAns.: Opens file, reads it and closes. Reads and closes again after closing the first which would give an error since it would have to be opened again")
in_file = open("inFile.txt",'r')
indata = in_file.read()
in_file.close() 
# Last two lines deleted since they aren't supposed to exist

# 3) 
print('\nExercise 1.3')
print("\nAns.: Reads first line of file twice\n")
in_file = open("inFile.txt",'r')
print(in_file.readline())
in_file = open("inFile.txt",'r')
print(in_file.readline())
in_file.close()

# ----------------------------
# Exercise 2.
"""
Escreva um programa que leia um ficheiro de texto linha a linha e escreva o seu conteúdo no
ecrã.
"""
print('\nExercise 2')
def reader(file):
    read_file = open(file, 'r')
    for line in read_file:
        print(line)
reader("inFile.txt")
# ----------------------------
# Exercise 3.
"""
Modifique o programa da alínea anterior de forma a aparecer também o número de cada
linha.
"""
print('\nExercise 3')
def reader(file):
    read_file = open(file, 'r')
    line_count = 0
    for line in read_file:
        print(line_count, line)
        line_count += 1
reader("inFile.txt")

# ----------------------------
# Exercise 4.

from medias import medias, medias1
print('\nExercise 4, solution 1:')
medias1("temperaturas.txt")
print('\nExercise 4, solution 2:')
medias("temperaturas.txt")

# ----------------------------
# Exercise 5.

"""
5. Escreva uma função principal sem parâmetros que pede ao utilizador o nome dum ficheiro
de temperaturas, e chama a função medias passando o nome do ficheiro. Caso ocorra algum
problema de acesso ao ficheiro, a função principal deve escrever Erro de I/O ao ler o ficheiro
"""

print('\nExercise 5')
      
def ficheiro_falha():
    user_input = input("indique o nome do ficheiro:")
    try:
        medias(user_input)
    except:
        print("não foi possível abrir o ficheiro")

ficheiro_falha()
        
# ----------------------------
# Exercise 6.
"""
6. Informação referente a símbolos químicos pode ser guardada em ficheiros de texto. Neste
caso estamos interessados apenas no nome, número atómico e densidade (em g/dm3 ). Eis
um exemplo:
Hélio 2 0.1786
Néon 10 0.9002
Argon 18 1.7818
Cripton 36 3.708
Xenon 54 5.851
Radônio 86 9.97
Hélio 2 0.1786 Néon 10 0.9002 Argon 18 1.7818 Cripton 36 3.708 Xenon 54 5.851 Radônio
86 9.97 2Escreva uma função linha_para_elemento que dada uma linha de um ficheiro de
elementos (uma string), produz um dicionário com chaves 'nome', 'atomico' e 'densidade'. O
nome é do tipo string, o número atómico do tipo int e a densidade do tipo float.
"""

print('\nExercise 6')

def linha_para_elemento(ficheiro):
    with open(ficheiro) as file_reader:
        list_of_dict_elements = []
        for line in file_reader:
            split_line = line.split()
            elements_temp_dict = {'nome':str(split_line[0]), 'atomico':int(split_line[1]), 'densidade':float(split_line[2])}
            list_of_dict_elements.append(elements_temp_dict)
    return list_of_dict_elements
# list_of_dict_elements.append(elements_temp_dict)
#      list_of_dict_elements
print(linha_para_elemento("C:\Users\criso\OneDrive\Documentos\Repos\pythoncode\Week_3\Class_14_29-10-2020\Ficha_4\elementos_quimicos.txt"))

linha_para_elemento("C:\Users\criso\OneDrive\Documentos\Repos\pythoncode\Week_3\Class_14_29-10-2020\Ficha_4\elementos_quimicos.txt")

# ----------------------------
# Exercise 7.
"""
7. Utilizando a função elemento_para_string, escreva uma função escrever_elementos que,
dada uma lista de dicionários representando elementos químicos e o nome de um ficheiro,
escreve o conteúdo da lista no ficheiro.
"""
print('\nExercise 7')

def escrever_elementos(lista, nome_ficheiro):
    with open(nome_ficheiro, 'w') as file_write:
        for i in lista:
            file_write.write(f"{i['nome']} {i['atomico']} {i['densidade']}\n")
            print(f"Linha adicionada ao ficheiro {nome_ficheiro}: \n{i['nome']} {i['atomico']} {i['densidade']}\n")
escrever_elementos(linha_para_elemento("elementos_quimicos.txt"), 'teste.txt')

# ----------------------------
# Exercise 8.
"""
8. O arranjo de átomos em moléculas pode ser descrito em formato textual, e portanto
guardado em ficheiros. Cada molécula é descrita por um número variável de linhas: a
primeira contém o nome da molécula, as subsequentes contêm informação sobre o átomo.
A última linha, END, fecha a molécula. Cada átomo é composto pelo seu identificador,
símbolo químico, e as coordenadas tri-dimensionais. Eis o exemplo de um ficheiro contendo
duas moléculas.
"""
print('\nExercise 8')

def linha_para_atomo(line):
    atom_dict = dict()
    split_line = line.split()
    if split_line[0] == 'ATOM':
        atom_dict['símbolo'] = split_line[2]
        atom_dict['id'] = split_line[1]
        atom_dict['x'] = split_line[3]
        atom_dict['y'] = split_line[3]
        atom_dict['z'] = split_line[3]
        return atom_dict
    
# Utilização da função
with open("atoms.txt") as file_reader:
    atom_list = []
    for line in file_reader:
        if linha_para_atomo(line) != None:
            atom_list.append(linha_para_atomo(line))
    print(atom_list)
            


            