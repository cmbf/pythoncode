6# -*- coding: utf-8 -*-
# ----------------------------
# @author: Cristiano Ferro (utilizador)
# Create on Fri Oct 30 09:32:31 2020
# ----------------------------
# FICHA 4 - Manipulação de CSV
# ----------------------------
# Exercício 1.

import csv

# Example
with open("eggs.csv", newline='') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
    print(spamreader.__next__())

# Solution 1
def le_graficos(ficheiro_csv):
    grafico = []
    with open(ficheiro_csv, newline='') as csvfile:
        csv_reader = csv.reader(csvfile, delimiter=';', quotechar='|')
        line1 = csv_reader.__next__()
        line2 = csv_reader.__next__()
        for i in range(len(line1)):
            item_linha1_com_ponto = line1[i].replace(',', '.')
            item_linha2_com_ponto = line2[i].replace(',', '.')
            grafico.append((float(item_linha1_com_ponto), float(item_linha2_com_ponto)))
    return grafico

print(le_graficos("graficos.csv"))
  

# Solution 2

def converte_para_notacao_ponto(item1, item2):
    return float(item1.replace(',', '.')), float(item2.replace(',', '.'))

def le_graficos_2(ficheiro_csv):
    grafico = []
    with open(ficheiro_csv, newline='') as csvfile:
        csv_reader = csv.reader(csvfile, delimiter=';', quotechar='|')
        line1 = csv_reader.__next__()
        line2 = csv_reader.__next__()
        # print(converte_para_notacao_ponto, line1, line2)
        grafico = list(map(converte_para_notacao_ponto, line1, line2))
    return grafico

print(le_graficos_2("graficos.csv"))

# ----------------------------
# Exercício 2.
# Pressupondo que a primeira lista lista_grafico[0] são as abcissas 
# e a segunda lista lista_grafico[1] as ordenadas

lista_grafico = [[0, 1, 2, 3, 4, 5], [0, 2, 4, 6, 8, 10]]
def graficoParaCsvLinha(nome_de_ficheiro, grafico):
    # lista(lista, lista)
    import csv
    with open(nome_de_ficheiro, 'w', newline='') as ficheiro_csv:
        writer = csv.writer(ficheiro_csv)
        writer.writerow(grafico[0])
        writer.writerow(grafico[1])

graficoParaCsvLinha("ficheiro.csv", lista_grafico)

# ----------------------------
# Exercício 3.

"""
3. Escreva uma função graficosParaCsv que, dado o nome de um ficheiro e uma lista de
gráficos (uma lista de listas de números), escreva os gráficos num ficheiro CSV. Organize a
informação no ficheiro do seguinte modo: a primeira coluna contém as abcissas, a segunda
coluna contém as ordenadas da primeira função, a terceira coluna contém as ordenadas da
segunda função, e por aí em diante. Assuma que todas as funções têm listas de abcissas
iguais
"""
# Presumindo que o que é para fazer é uma lista com n listas 2 listas respetivamente 
# de abcissas e ordenadas. As abcissas apenas serão usadas 1x. Depois só as ordenadas

lista_grafico = [[[0, 1, 2, 3, 4, 5], [0, 2, 4, 6, 8, 10]],
                 [[0, 1, 2, 3, 4, 5], [0, 6, 8, 11, 17, 18]],
                 [[0, 1, 2, 3, 4, 5], [0, 2, 4, 6, 8, 20]],
                 ]

def graficosParaCsv(nome_de_ficheiro, grafico):
    # header = [abcissas, ordenadas, ordenadas2]
    # lista_de_graficos = [                                 
    #                         [[abcissas],[ordenadas]],     # [lista_de_graficos[0][0], lista_de_graficos[0][1]]
    #                         [[abcissas],[ordenadas]],     # [lista_de_graficos[1][0], lista_de_graficos[1][1]]
    #                         [[abcissas],[ordenadas],]     # [lista_de_graficos[2][0], lista_de_graficos[2][1]]
    #                     ]
    with open(nome_de_ficheiro, 'w', newline='') as ficheiro_csv:
        writer = csv.writer(ficheiro_csv)
        writer.writerow(["Abcissas","Ordenadas"])
        for graph in range(len(grafico)):
            if graph == 0:
                pass
            else:
                writer.writerow([f"Abcissas {graph}",f"Ordenadas {graph}"])
            for i in range(len(grafico[0][0])):
                writer.writerow([i, grafico[graph][1][i]])
        
graficosParaCsv('teste_ex3.csv', lista_grafico)



