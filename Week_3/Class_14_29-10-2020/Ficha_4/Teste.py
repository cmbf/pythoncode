# -*- coding: utf-8 -*-
"""
@author: Cristiano Ferro %(username)
Created on %(date) 
"""
# ----------------------------
def linha_para_elemento(ficheiro):
    # Acessar elementos do ficheiro
    # elementos = dict()
    # 
    # Retirar espaços etc, colocar em lista?
    # 1o elemento da lista é a key, 
    # elementos = {'nome':'Helio', 'atomico':'2' 'densidade':'0.1786' }
    # lista_de elementos:
    with open(ficheiro) as file_reader:
        list_of_dict_elements = []
        for line in file_reader:
            split_line = line.split()
            elements_temp_dict = {'nome':str(split_line[0]), 'atomico':int(split_line[1]), 'densidade':float(split_line[2])}
            list_of_dict_elements.append(elements_temp_dict)
    return list_of_dict_elements

print(linha_para_elemento("elementos_quimicos.txt"))
